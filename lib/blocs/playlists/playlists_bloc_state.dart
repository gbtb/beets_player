import 'package:beets_player/blocs/playlist/playlist_bloc.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
part 'playlists_bloc_state.g.dart';

abstract class PlaylistsBlocState implements Built<PlaylistsBlocState, PlaylistsBlocStateBuilder> {
  BuiltList<PlaylistBloc> get playlists;

  /// Currently selected playlist (in UI)
  @nullable
  PlaylistBloc get currentPlaylist;

  PlaylistsBlocState._();
  factory PlaylistsBlocState([void Function(PlaylistsBlocStateBuilder) updates]) = _$PlaylistsBlocState;
}
