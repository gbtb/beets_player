import 'package:beets_player/blocs/bloc_factory.dart';
import 'package:beets_player/blocs/player/player_bloc.dart';
import 'package:beets_player/blocs/playlist/playlist_bloc.dart';
import 'package:beets_player/core/models/library_item.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import 'playlists_bloc_state.dart';

/// Responsible for storing and managing playlists collection
class PlaylistsBloc extends Bloc<PlaylistsBlocEvent, PlaylistsBlocState> {
  final PlayerBloc playerBloc;
  PlaylistsBloc(this.playerBloc);

  @override
  get initialState => PlaylistsBlocState((b) => b..playlists.build());

  @override
  Stream<PlaylistsBlocState> mapEventToState(event) async* {
    if (event is CreateNewPlaylist) {
      final playlist = blocFactory.getPlaylistBloc(event.name, event.items);
      playlist.dispatch(MoveSelectionUp()); //initializing selected item in new playlist
      yield this.currentState.rebuild((b) => b
        ..playlists.add(playlist)
        ..currentPlaylist = playlist);
    } else if (event is CreateNewPlaylistAndStartPlaying) {
      final newPlaylist = blocFactory.getPlaylistBloc(event.name, event.items);
      yield this.currentState.rebuild((b) => b
        ..playlists.add(newPlaylist)
        ..currentPlaylist = newPlaylist);
      playerBloc.dispatch(SetPlaylist(newPlaylist));
      playerBloc.dispatch(Play());
    } else if (event is SetSelectedPlaylist) {
      var selected = this.currentState.playlists.firstWhere((p) => p == event.playlist, orElse: null);
      //TODO: not found
      yield this.currentState.rebuild((b) => b..currentPlaylist = selected);
    } else if (event is AppendToCurrentPlaylist) {
      this.currentState.currentPlaylist.dispatch(AddItemsToPlaylist(event.items));
    } else if (event is ClosePlaylist) {
      final builder = this.currentState.toBuilder();
      //TODO: not found
      builder..playlists.remove(event.playlist);
      if (this.currentState.currentPlaylist == event.playlist) builder..currentPlaylist = null;

      yield builder.build();
    }
  }
}

abstract class PlaylistsBlocEvent {}

@immutable
class CreateNewPlaylist implements PlaylistsBlocEvent {
  final Iterable<LibraryItem> items;
  final String name;

  CreateNewPlaylist(this.name, this.items);
}

@immutable
class CreateNewPlaylistAndStartPlaying implements PlaylistsBlocEvent {
  final Iterable<LibraryItem> items;
  final String name;

  CreateNewPlaylistAndStartPlaying(this.name, this.items);
}

@immutable
class SetSelectedPlaylist implements PlaylistsBlocEvent {
  final PlaylistBloc playlist;

  SetSelectedPlaylist(this.playlist);
}

@immutable
class AppendToCurrentPlaylist implements PlaylistsBlocEvent {
  final Iterable<LibraryItem> items;

  AppendToCurrentPlaylist(this.items);
}

@immutable
class ClosePlaylist implements PlaylistsBlocEvent {
  final PlaylistBloc playlist;
  ClosePlaylist(this.playlist);
}
