// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'playlists_bloc_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PlaylistsBlocState extends PlaylistsBlocState {
  @override
  final BuiltList<PlaylistBloc> playlists;
  @override
  final PlaylistBloc currentPlaylist;

  factory _$PlaylistsBlocState(
          [void Function(PlaylistsBlocStateBuilder) updates]) =>
      (new PlaylistsBlocStateBuilder()..update(updates)).build();

  _$PlaylistsBlocState._({this.playlists, this.currentPlaylist}) : super._() {
    if (playlists == null) {
      throw new BuiltValueNullFieldError('PlaylistsBlocState', 'playlists');
    }
  }

  @override
  PlaylistsBlocState rebuild(
          void Function(PlaylistsBlocStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PlaylistsBlocStateBuilder toBuilder() =>
      new PlaylistsBlocStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PlaylistsBlocState &&
        playlists == other.playlists &&
        currentPlaylist == other.currentPlaylist;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, playlists.hashCode), currentPlaylist.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PlaylistsBlocState')
          ..add('playlists', playlists)
          ..add('currentPlaylist', currentPlaylist))
        .toString();
  }
}

class PlaylistsBlocStateBuilder
    implements Builder<PlaylistsBlocState, PlaylistsBlocStateBuilder> {
  _$PlaylistsBlocState _$v;

  ListBuilder<PlaylistBloc> _playlists;
  ListBuilder<PlaylistBloc> get playlists =>
      _$this._playlists ??= new ListBuilder<PlaylistBloc>();
  set playlists(ListBuilder<PlaylistBloc> playlists) =>
      _$this._playlists = playlists;

  PlaylistBloc _currentPlaylist;
  PlaylistBloc get currentPlaylist => _$this._currentPlaylist;
  set currentPlaylist(PlaylistBloc currentPlaylist) =>
      _$this._currentPlaylist = currentPlaylist;

  PlaylistsBlocStateBuilder();

  PlaylistsBlocStateBuilder get _$this {
    if (_$v != null) {
      _playlists = _$v.playlists?.toBuilder();
      _currentPlaylist = _$v.currentPlaylist;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PlaylistsBlocState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PlaylistsBlocState;
  }

  @override
  void update(void Function(PlaylistsBlocStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PlaylistsBlocState build() {
    _$PlaylistsBlocState _$result;
    try {
      _$result = _$v ??
          new _$PlaylistsBlocState._(
              playlists: playlists.build(), currentPlaylist: currentPlaylist);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'playlists';
        playlists.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PlaylistsBlocState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
