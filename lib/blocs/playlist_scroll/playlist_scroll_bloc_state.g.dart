// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'playlist_scroll_bloc_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PlaylistScrollBlocState extends PlaylistScrollBlocState {
  @override
  final double listItemHeight;
  @override
  final int scrolledToIndex;
  @override
  final mat.ScrollController controller;
  bool __canScroll;

  factory _$PlaylistScrollBlocState(
          [void Function(PlaylistScrollBlocStateBuilder) updates]) =>
      (new PlaylistScrollBlocStateBuilder()..update(updates)).build();

  _$PlaylistScrollBlocState._(
      {this.listItemHeight, this.scrolledToIndex, this.controller})
      : super._();

  @override
  bool get canScroll => __canScroll ??= super.canScroll;

  @override
  PlaylistScrollBlocState rebuild(
          void Function(PlaylistScrollBlocStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PlaylistScrollBlocStateBuilder toBuilder() =>
      new PlaylistScrollBlocStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PlaylistScrollBlocState &&
        listItemHeight == other.listItemHeight &&
        scrolledToIndex == other.scrolledToIndex &&
        controller == other.controller;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, listItemHeight.hashCode), scrolledToIndex.hashCode),
        controller.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PlaylistScrollBlocState')
          ..add('listItemHeight', listItemHeight)
          ..add('scrolledToIndex', scrolledToIndex)
          ..add('controller', controller))
        .toString();
  }
}

class PlaylistScrollBlocStateBuilder
    implements
        Builder<PlaylistScrollBlocState, PlaylistScrollBlocStateBuilder> {
  _$PlaylistScrollBlocState _$v;

  double _listItemHeight;
  double get listItemHeight => _$this._listItemHeight;
  set listItemHeight(double listItemHeight) =>
      _$this._listItemHeight = listItemHeight;

  int _scrolledToIndex;
  int get scrolledToIndex => _$this._scrolledToIndex;
  set scrolledToIndex(int scrolledToIndex) =>
      _$this._scrolledToIndex = scrolledToIndex;

  mat.ScrollController _controller;
  mat.ScrollController get controller => _$this._controller;
  set controller(mat.ScrollController controller) =>
      _$this._controller = controller;

  PlaylistScrollBlocStateBuilder();

  PlaylistScrollBlocStateBuilder get _$this {
    if (_$v != null) {
      _listItemHeight = _$v.listItemHeight;
      _scrolledToIndex = _$v.scrolledToIndex;
      _controller = _$v.controller;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PlaylistScrollBlocState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PlaylistScrollBlocState;
  }

  @override
  void update(void Function(PlaylistScrollBlocStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PlaylistScrollBlocState build() {
    final _$result = _$v ??
        new _$PlaylistScrollBlocState._(
            listItemHeight: listItemHeight,
            scrolledToIndex: scrolledToIndex,
            controller: controller);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
