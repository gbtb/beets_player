import 'package:built_value/built_value.dart';
import 'package:flutter/material.dart' as mat;
part 'playlist_scroll_bloc_state.g.dart';

abstract class PlaylistScrollBlocState implements Built<PlaylistScrollBlocState, PlaylistScrollBlocStateBuilder> {
  @nullable
  double get listItemHeight;

  @nullable
  int get scrolledToIndex;

  @nullable
  mat.ScrollController get controller;

  @memoized
  bool get canScroll => controller != null && listItemHeight != null;

  PlaylistScrollBlocState._();
  factory PlaylistScrollBlocState([void Function(PlaylistScrollBlocStateBuilder) updates]) = _$PlaylistScrollBlocState;
}
