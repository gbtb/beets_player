import 'package:beets_player/blocs/base_bloc.dart';
import 'package:beets_player/blocs/error/error_bloc.dart';
import 'package:beets_player/core/sdk/app_error.dart';
import 'package:beets_player/core/sdk/time/time_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'playlist_scroll_bloc_state.dart';

///This class exists only because flutter has no built-in support for jump-to-item inside list-view. niiiiiiiiiiiiiiiiiiceeeeeeeeeeeee
class PlaylistScrollBloc extends BaseBloc<PlaylistScrollEvent, PlaylistScrollBlocState> {
  ScrollController _controller;

  PlaylistScrollBloc(ScrollController controller, ErrorBloc errorBloc, ITimeProvider provider)
      : super(errorBloc, provider) {
    _controller = controller;
  }

  @override
  PlaylistScrollBlocState get initialState => PlaylistScrollBlocState((b) => b
    ..controller = _controller
    ..scrolledToIndex = 0);

  @override
  Stream<PlaylistScrollBlocState> mapEventToState(
    PlaylistScrollEvent event,
  ) async* {
    if (event is Init) {
      yield initialState;
    }
    if (event is KeyToGetItemSize) {
      try {
        final RenderBox renderBox = event.key.currentContext.findRenderObject();
        final height = renderBox.size.height;
        if (height == null || height <= 0)
          reportError(ScrollDisabledError("Can't find list item height - scroll helper disabled"));
        yield currentState.rebuild((b) => b.listItemHeight = height);
      } on Exception catch (e) {
        reportError(CaughtException(e));
      }
    } else if (event is SetSelectedItem) {
      if (currentState.scrolledToIndex == event.index) return;

      if (!currentState.canScroll) reportInfo("Can't auto-scroll due to failed controller init");

      final jumpDist = event.index * currentState.listItemHeight - currentState.controller.offset;
      currentState.controller.jumpTo(jumpDist);
      currentState.rebuild((b) => b..scrolledToIndex = event.index);
      reportDebug("Jumped $jumpDist");
    } else if (event is MoveSelectionDown) {
      scrollAfterMove(currentState.scrolledToIndex + 1, currentState.controller.offset + currentState.listItemHeight);
    } else if (event is MoveSelectionUp) {
      scrollAfterMove(currentState.scrolledToIndex - 1, currentState.controller.offset - currentState.listItemHeight);
    }
  }

  void scrollAfterMove(int idx, double jumpDestination) {
    if (!currentState.canScroll) reportInfo("Can't auto-scroll due to failed controller init");

    currentState.controller.jumpTo(jumpDestination);
    reportDebug("Jumped $jumpDestination");
  }

  @override
  String get blocRepresentation => "Playlist scrolling controller";
}

@immutable
abstract class PlaylistScrollEvent {}

@immutable
class Init extends PlaylistScrollEvent {}

@immutable
class MoveSelectionUp implements PlaylistScrollEvent {}

@immutable
class SetSelectedItem implements PlaylistScrollEvent {
  final int index;

  SetSelectedItem(this.index);
}

@immutable
class MoveSelectionDown implements PlaylistScrollEvent {}

@immutable
class KeyToGetItemSize implements PlaylistScrollEvent {
  final GlobalKey key;

  KeyToGetItemSize(this.key);
}

class ScrollDisabledError extends AppError {
  final String _message;

  ScrollDisabledError(this._message);

  @override
  // TODO: implement message
  String get message => _message;
}
