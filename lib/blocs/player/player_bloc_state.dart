import 'package:beets_player/blocs/player/playback_state.dart';
import 'package:beets_player/blocs/playlist/playlist_bloc.dart';
import 'package:beets_player/core/models/library_item.dart';
import 'package:beets_player/core/models/volume_level.dart';
import 'package:built_value/built_value.dart';

part 'player_bloc_state.g.dart';

abstract class PlayerBlocState implements Built<PlayerBlocState, PlayerBlocStateBuilder> {
  @nullable
  LibraryItem get currentItem;
  @nullable
  PlaylistBloc get playlistBloc;

  PlaybackState get playbackState;

  @nullable
  Duration get remainedDuration;

  VolumeLevel get volume;

  bool get isMuted;

  PlayerBlocState._();
  factory PlayerBlocState([void Function(PlayerBlocStateBuilder) updates]) = _$PlayerBlocState;
}
