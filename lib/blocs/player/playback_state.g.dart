// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'playback_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const PlaybackState _$unknown = const PlaybackState._('Unknown');
const PlaybackState _$playing = const PlaybackState._('Playing');
const PlaybackState _$stopped = const PlaybackState._('Stopped');
const PlaybackState _$paused = const PlaybackState._('Paused');

PlaybackState _$valueOf(String name) {
  switch (name) {
    case 'Unknown':
      return _$unknown;
    case 'Playing':
      return _$playing;
    case 'Stopped':
      return _$stopped;
    case 'Paused':
      return _$paused;
    default:
      return _$unknown;
  }
}

final BuiltSet<PlaybackState> _$values =
    new BuiltSet<PlaybackState>(const <PlaybackState>[
  _$unknown,
  _$playing,
  _$stopped,
  _$paused,
]);

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
