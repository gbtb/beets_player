// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'player_bloc_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PlayerBlocState extends PlayerBlocState {
  @override
  final LibraryItem currentItem;
  @override
  final PlaylistBloc playlistBloc;
  @override
  final PlaybackState playbackState;
  @override
  final Duration remainedDuration;
  @override
  final VolumeLevel volume;
  @override
  final bool isMuted;

  factory _$PlayerBlocState([void Function(PlayerBlocStateBuilder) updates]) =>
      (new PlayerBlocStateBuilder()..update(updates)).build();

  _$PlayerBlocState._(
      {this.currentItem,
      this.playlistBloc,
      this.playbackState,
      this.remainedDuration,
      this.volume,
      this.isMuted})
      : super._() {
    if (playbackState == null) {
      throw new BuiltValueNullFieldError('PlayerBlocState', 'playbackState');
    }
    if (volume == null) {
      throw new BuiltValueNullFieldError('PlayerBlocState', 'volume');
    }
    if (isMuted == null) {
      throw new BuiltValueNullFieldError('PlayerBlocState', 'isMuted');
    }
  }

  @override
  PlayerBlocState rebuild(void Function(PlayerBlocStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PlayerBlocStateBuilder toBuilder() =>
      new PlayerBlocStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PlayerBlocState &&
        currentItem == other.currentItem &&
        playlistBloc == other.playlistBloc &&
        playbackState == other.playbackState &&
        remainedDuration == other.remainedDuration &&
        volume == other.volume &&
        isMuted == other.isMuted;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, currentItem.hashCode), playlistBloc.hashCode),
                    playbackState.hashCode),
                remainedDuration.hashCode),
            volume.hashCode),
        isMuted.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PlayerBlocState')
          ..add('currentItem', currentItem)
          ..add('playlistBloc', playlistBloc)
          ..add('playbackState', playbackState)
          ..add('remainedDuration', remainedDuration)
          ..add('volume', volume)
          ..add('isMuted', isMuted))
        .toString();
  }
}

class PlayerBlocStateBuilder
    implements Builder<PlayerBlocState, PlayerBlocStateBuilder> {
  _$PlayerBlocState _$v;

  LibraryItemBuilder _currentItem;
  LibraryItemBuilder get currentItem =>
      _$this._currentItem ??= new LibraryItemBuilder();
  set currentItem(LibraryItemBuilder currentItem) =>
      _$this._currentItem = currentItem;

  PlaylistBloc _playlistBloc;
  PlaylistBloc get playlistBloc => _$this._playlistBloc;
  set playlistBloc(PlaylistBloc playlistBloc) =>
      _$this._playlistBloc = playlistBloc;

  PlaybackState _playbackState;
  PlaybackState get playbackState => _$this._playbackState;
  set playbackState(PlaybackState playbackState) =>
      _$this._playbackState = playbackState;

  Duration _remainedDuration;
  Duration get remainedDuration => _$this._remainedDuration;
  set remainedDuration(Duration remainedDuration) =>
      _$this._remainedDuration = remainedDuration;

  VolumeLevelBuilder _volume;
  VolumeLevelBuilder get volume => _$this._volume ??= new VolumeLevelBuilder();
  set volume(VolumeLevelBuilder volume) => _$this._volume = volume;

  bool _isMuted;
  bool get isMuted => _$this._isMuted;
  set isMuted(bool isMuted) => _$this._isMuted = isMuted;

  PlayerBlocStateBuilder();

  PlayerBlocStateBuilder get _$this {
    if (_$v != null) {
      _currentItem = _$v.currentItem?.toBuilder();
      _playlistBloc = _$v.playlistBloc;
      _playbackState = _$v.playbackState;
      _remainedDuration = _$v.remainedDuration;
      _volume = _$v.volume?.toBuilder();
      _isMuted = _$v.isMuted;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PlayerBlocState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PlayerBlocState;
  }

  @override
  void update(void Function(PlayerBlocStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PlayerBlocState build() {
    _$PlayerBlocState _$result;
    try {
      _$result = _$v ??
          new _$PlayerBlocState._(
              currentItem: _currentItem?.build(),
              playlistBloc: playlistBloc,
              playbackState: playbackState,
              remainedDuration: remainedDuration,
              volume: volume.build(),
              isMuted: isMuted);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'currentItem';
        _currentItem?.build();

        _$failedField = 'volume';
        volume.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PlayerBlocState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
