import 'dart:async';

import 'package:beets_player/blocs/error/error_bloc.dart';
import 'package:beets_player/blocs/player/playback_state.dart';
import 'package:beets_player/blocs/player/player_bloc_state.dart';
import 'package:beets_player/blocs/playlist/playlist_bloc.dart';
import 'package:beets_player/core/models/library_item.dart';
import 'package:beets_player/core/models/volume_level.dart';
import 'package:beets_player/core/playback_service.dart';
import 'package:beets_player/core/sdk/time/time_provider.dart';
import 'package:flutter/widgets.dart';

import '../base_bloc.dart';

class PlayerBloc extends BaseBloc<PlayerEvent, PlayerBlocState> {
  final IPlaybackService _playbackService;
  Timer _timer;
  Stopwatch _stopwatch;
  PlayerBloc(this._playbackService, ErrorBloc _errorBloc, ITimeProvider timeProvider)
      : super(_errorBloc, timeProvider) {
    _stopwatch = timeProvider.getStopwatch();
  }

  @override
  PlayerBlocState get initialState => PlayerBlocState((b) => b
    ..playbackState = PlaybackState.Unknown
    ..volume.replace(VolumeLevel((b) {
      b..volume = 50;
    }))
    ..isMuted = false);

  @override
  Stream<PlayerBlocState> mapEventToState(event) async* {
    if (event is TogglePlay) {
      if (currentState.playbackState == PlaybackState.Playing) {
        var newState = await _handlePause();
        yield newState.build();
      } else {
        if (this.currentState.currentItem == null) return;

        final newState = await _startPlaying(this.currentState.currentItem);
        yield newState.build();
      }
    } else if (event is SetPlaylist) {
      var nextItem = event._playlistBloc.getNextItem(null);
      if (nextItem != null) {
        final res = await _playbackService.setItem(nextItem);
        if (res.err != null) {
          reportError(res.err);
          return;
        }
        yield this.currentState.rebuild((b) => b
          ..currentItem.replace(nextItem)
          ..playbackState = PlaybackState.Stopped
          ..playlistBloc = event._playlistBloc
          ..remainedDuration = nextItem.duration);
      }
    } else if (event is SetPlaylistAndItem) {
      final newState = await _handleStopEvent();

      if (event.playlistBloc != currentState.playlistBloc) newState..playlistBloc = event.playlistBloc;

      if (event.item != currentState.currentItem) {
        newState
          ..currentItem.replace(event.item)
          ..remainedDuration = event.item.duration;

        final res = await _playbackService.setItem(event.item);
        if (res.err != null) {
          reportError(res.err);
          return;
        }
      }
      yield newState.build();
    } else if (event is Play) {
      if (this.currentState.currentItem == null) return;

      final newState = await _startPlaying(this.currentState.currentItem);
      yield newState.build();
    } else if (event is TimerTick) {
      final shouldStop = this.currentState.remainedDuration <= Duration.zero;
      if (shouldStop) {
        var newState = await _handleStopTick();
        yield newState.build();
        return;
      }
      yield this
          .currentState
          .rebuild((b) => b..remainedDuration = this.currentState.currentItem.duration - _stopwatch.elapsed);
    } else if (event is Pause) {
      var newState = await _handlePause();
      yield newState.build();
    } else if (event is Stop) {
      final newState = await _handleStopEvent();
      yield newState.build();
    } else if (event is SetVolume) {
      final res = await _playbackService.setVolume(event.level);
      if (res.isOk())
        yield currentState.rebuild((b) => b..volume.replace(event.level));
      else
        reportError(res.err);
    } else if (event is SetMuted) {
      final res = await _playbackService.setMuted(event.muted);
      if (res.isOk())
        yield currentState.rebuild((b) => b..isMuted = event.muted);
      else
        reportError(res.err);
    }
  }

  Future<PlayerBlocStateBuilder> _handlePause() async {
    if (this.currentState.currentItem != null && this.currentState.playbackState == PlaybackState.Playing) {
      final res = await _playbackService.pause();
      _stopwatch.stop();
      _timer.cancel();
      if (res.err != null) {
        reportError(res.err);
        return currentState.toBuilder()..playbackState = PlaybackState.Unknown;
      }

      return this.currentState.toBuilder()
        ..playbackState = PlaybackState.Paused
        ..remainedDuration = this.currentState.currentItem.duration - _stopwatch.elapsed;
    }

    return currentState.toBuilder();
  }

  Future<PlayerBlocStateBuilder> _handleStopEvent() async {
    if (this.currentState.currentItem == null) return currentState.toBuilder();
    var state = currentState.toBuilder();
    state
      ..playbackState = PlaybackState.Stopped
      ..remainedDuration = currentState.currentItem.duration;

    final res = await _playbackService.stop();
    if (res.err != null) {
      reportError(res.err);
      return state..playbackState = PlaybackState.Unknown;
    }

    _timer.cancel();
    _stopwatch.stop();
    _stopwatch.reset();
    return state;
  }

  Future<PlayerBlocStateBuilder> _handleStopTick() async {
    _timer?.cancel();
    _stopwatch.stop();
    _stopwatch.reset();
    final nextItem = this.currentState.playlistBloc.getNextItem(this.currentState.currentItem);
    var newState = this.currentState.toBuilder();

    if (nextItem == null) {
      final res = await _playbackService.stop();
      if (res.err != null) {
        reportError(res.err);
        newState..playbackState = PlaybackState.Unknown;
        return newState;
      }

      return newState
        ..currentItem = null
        ..playbackState = PlaybackState.Stopped
        ..remainedDuration = Duration.zero;
    }

    await _playbackService.setItem(nextItem);
    newState = await _startPlaying(nextItem);
    newState..currentItem.replace(nextItem);
    return newState;
  }

  Future<PlayerBlocStateBuilder> _startPlaying(LibraryItem item) async {
    final res = await _playbackService.play();
    //Bloc lib ignores same state
    if (res.err != null) {
      reportError(res.err);
      return this.currentState.toBuilder()..playbackState = PlaybackState.Unknown;
    }

    _stopwatch.start();
    _timer = timeProvider.periodic(Duration(seconds: 1), (_) => this.dispatch(TimerTick()));
    return this.currentState.toBuilder()
      ..playbackState = PlaybackState.Playing
      ..remainedDuration = item.duration;
  }

  @override
  String get blocRepresentation => "Player";
}

abstract class PlayerEvent {}

@immutable
class SetPlaylist extends PlayerEvent {
  final PlaylistBloc _playlistBloc;

  SetPlaylist(this._playlistBloc);
}

@immutable
class Play extends PlayerEvent {}

@immutable
class Pause extends PlayerEvent {}

@immutable
class Stop extends PlayerEvent {}

@immutable
class TimerTick extends PlayerEvent {}

@immutable
class SetPlaylistAndItem extends PlayerEvent {
  final PlaylistBloc playlistBloc;
  final LibraryItem item;

  SetPlaylistAndItem(this.playlistBloc, this.item);
}

@immutable
class SetVolume extends PlayerEvent {
  final VolumeLevel level;

  SetVolume(this.level);
}

@immutable
class SetMuted extends PlayerEvent {
  final bool muted;

  SetMuted(this.muted);
}

@immutable
class TogglePlay extends PlayerEvent {}
