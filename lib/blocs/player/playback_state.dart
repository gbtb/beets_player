import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
part 'playback_state.g.dart';

class PlaybackState extends EnumClass {
  @BuiltValueEnumConst(fallback: true)
  static const PlaybackState Unknown = _$unknown;
  static const PlaybackState Playing = _$playing;
  static const PlaybackState Stopped = _$stopped;
  static const PlaybackState Paused = _$paused;

  const PlaybackState._(String name) : super(name);
  static BuiltSet<PlaybackState> get values => _$values;

  static PlaybackState valueOf(String name) => _$valueOf(name);
}
