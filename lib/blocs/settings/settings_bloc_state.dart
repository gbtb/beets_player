import 'package:beets_player/core/models/settings/general_settings.dart';
import 'package:built_value/built_value.dart';
part 'settings_bloc_state.g.dart';

abstract class SettingsBlocState implements Built<SettingsBlocState, SettingsBlocStateBuilder> {
  GeneralSettings get generalSettings;

  SettingsBlocState._();
  factory SettingsBlocState([void Function(SettingsBlocStateBuilder) updates]) = _$SettingsBlocState;
}
