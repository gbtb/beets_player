// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'settings_bloc_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SettingsBlocState extends SettingsBlocState {
  @override
  final GeneralSettings generalSettings;

  factory _$SettingsBlocState(
          [void Function(SettingsBlocStateBuilder) updates]) =>
      (new SettingsBlocStateBuilder()..update(updates)).build();

  _$SettingsBlocState._({this.generalSettings}) : super._() {
    if (generalSettings == null) {
      throw new BuiltValueNullFieldError(
          'SettingsBlocState', 'generalSettings');
    }
  }

  @override
  SettingsBlocState rebuild(void Function(SettingsBlocStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SettingsBlocStateBuilder toBuilder() =>
      new SettingsBlocStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SettingsBlocState &&
        generalSettings == other.generalSettings;
  }

  @override
  int get hashCode {
    return $jf($jc(0, generalSettings.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SettingsBlocState')
          ..add('generalSettings', generalSettings))
        .toString();
  }
}

class SettingsBlocStateBuilder
    implements Builder<SettingsBlocState, SettingsBlocStateBuilder> {
  _$SettingsBlocState _$v;

  GeneralSettingsBuilder _generalSettings;
  GeneralSettingsBuilder get generalSettings =>
      _$this._generalSettings ??= new GeneralSettingsBuilder();
  set generalSettings(GeneralSettingsBuilder generalSettings) =>
      _$this._generalSettings = generalSettings;

  SettingsBlocStateBuilder();

  SettingsBlocStateBuilder get _$this {
    if (_$v != null) {
      _generalSettings = _$v.generalSettings?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SettingsBlocState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SettingsBlocState;
  }

  @override
  void update(void Function(SettingsBlocStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SettingsBlocState build() {
    _$SettingsBlocState _$result;
    try {
      _$result = _$v ??
          new _$SettingsBlocState._(generalSettings: generalSettings.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'generalSettings';
        generalSettings.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'SettingsBlocState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
