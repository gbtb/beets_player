import 'package:beets_player/core/beets_config_repository.dart';
import 'package:beets_player/core/models/settings/general_settings.dart';
import 'package:beets_player/core/settings_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';

import 'settings_bloc_state.dart';

class SettingsBloc extends Bloc<SettingsBlocEvent, SettingsBlocState> {
  SettingsBlocState _initialState;

  ///Used to construct from known basic state
  SettingsBloc._(this._initialState);

  static Future<SettingsBloc> init(ISettingsRepository settingsRepository, IBeetsConfigRepository configRepo) async {
    final generalSettingsRes = await settingsRepository.getGeneralSettings();
    GeneralSettings generalSettings;
    if (generalSettingsRes.isOk()) {
      generalSettings = generalSettingsRes.ok;
    } else if (generalSettingsRes.err is SettingsNotFoundError) {
      generalSettings = await GeneralSettings.init(configRepo);
    } else {
      //todo: error logging
    }

    final state = SettingsBlocState((b) => b..generalSettings.replace(generalSettings));
    return SettingsBloc._(state);
  }

  @override
  SettingsBlocState get initialState => _initialState;

  @override
  Stream<SettingsBlocState> mapEventToState(
    SettingsBlocEvent event,
  ) async* {
    // TODO: implement mapEventToState
  }
}

@immutable
abstract class SettingsBlocEvent {}
