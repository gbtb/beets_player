import 'package:beets_player/adapters/config_repository.dart';
import 'package:beets_player/adapters/playback_service.dart';
import 'package:beets_player/adapters/search_repository.dart';
import 'package:beets_player/adapters/settings_repository.dart';
import 'package:beets_player/blocs/error/error_bloc.dart';
import 'package:beets_player/blocs/keyboard/keyboard_bloc.dart';
import 'package:beets_player/blocs/player/player_bloc.dart';
import 'package:beets_player/blocs/playlist/playlist_bloc.dart';
import 'package:beets_player/blocs/playlists/playlists_bloc.dart';
import 'package:beets_player/blocs/search/search_bloc.dart';
import 'package:beets_player/blocs/settings/settings_bloc.dart';
import 'package:beets_player/core/beets_config_repository.dart';
import 'package:beets_player/core/models/library_item.dart';
import 'package:beets_player/core/playback_service.dart';
import 'package:beets_player/core/sdk/app_error.dart';
import 'package:beets_player/core/sdk/result.dart';
import 'package:beets_player/core/sdk/time/time_provider.dart';
import 'package:beets_player/core/search_repository.dart';
import 'package:beets_player/core/settings_repository.dart';
import 'package:flutter/foundation.dart';

BlocFactory blocFactory = BlocFactory().init();

class BlocFactory {
  IBeetsConfigRepository _configRepository;

  Result<AppError, ISearchRepository> _searchRepo;

  SearchBloc _searchBloc;

  PlayerBloc _playerBloc;

  PlaylistsBloc _playlistsBloc;

  ErrorBloc _errorBloc;

  IPlaybackService _playbackService;

  RealTimeProvider _timeProvider;

  KeyboardBloc _keyboardBloc;

  SettingsBloc _settingsBloc;

  @visibleForTesting
  BlocFactory();

  BlocFactory init(
      {IBeetsConfigRepository configRepository,
      ISearchRepository searchRepository,
      ISettingsRepository settingsRepository}) {
    _errorBloc = ErrorBloc();
    _configRepository = configRepository ?? DesktopConfigRepository();
    settingsRepository = settingsRepository ?? SettingsRepository();

    //asynchronous init of settingsBloc and its dependencies. It doesnt look great, i know
    SettingsBloc.init(settingsRepository, _configRepository).then((settingsBloc) async {
      _settingsBloc = settingsBloc;
      var repo = searchRepository ?? BeetsSearchRepository();
      var res = await repo.init(_settingsBloc.currentState.generalSettings.beetsDatabaseUri);
      _searchBloc.dispatch(SetRepository(res));
    });

    _timeProvider = RealTimeProvider();
    _playbackService = PlaybackService();

    _searchBloc = SearchBloc(_errorBloc, _timeProvider);
    _playerBloc = PlayerBloc(_playbackService, _errorBloc, _timeProvider);
    _playlistsBloc = PlaylistsBloc(_playerBloc);
    _keyboardBloc = KeyboardBloc(_errorBloc, _timeProvider);

    return this;
  }

  SearchBloc getSearchBloc() => _searchBloc;

  PlaylistsBloc getPlaylistsBloc() => _playlistsBloc;

  ErrorBloc getErrorBloc() => _errorBloc;

  PlayerBloc getPlayerBloc() => _playerBloc;

  PlaylistBloc getPlaylistBloc(String name, Iterable<LibraryItem> items) =>
      PlaylistBloc(name, items, _playerBloc, _errorBloc, _timeProvider);

  KeyboardBloc getKeyboardBloc() => _keyboardBloc;

  SettingsBloc getSettingsBloc() => _settingsBloc;
}
