// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'error_bloc_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ErrorBlocState extends ErrorBlocState {
  @override
  final AppErrorEvent lastError;

  factory _$ErrorBlocState([void Function(ErrorBlocStateBuilder) updates]) =>
      (new ErrorBlocStateBuilder()..update(updates)).build();

  _$ErrorBlocState._({this.lastError}) : super._();

  @override
  ErrorBlocState rebuild(void Function(ErrorBlocStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ErrorBlocStateBuilder toBuilder() =>
      new ErrorBlocStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ErrorBlocState && lastError == other.lastError;
  }

  @override
  int get hashCode {
    return $jf($jc(0, lastError.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ErrorBlocState')
          ..add('lastError', lastError))
        .toString();
  }
}

class ErrorBlocStateBuilder
    implements Builder<ErrorBlocState, ErrorBlocStateBuilder> {
  _$ErrorBlocState _$v;

  AppErrorEvent _lastError;
  AppErrorEvent get lastError => _$this._lastError;
  set lastError(AppErrorEvent lastError) => _$this._lastError = lastError;

  ErrorBlocStateBuilder();

  ErrorBlocStateBuilder get _$this {
    if (_$v != null) {
      _lastError = _$v.lastError;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ErrorBlocState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ErrorBlocState;
  }

  @override
  void update(void Function(ErrorBlocStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ErrorBlocState build() {
    final _$result = _$v ?? new _$ErrorBlocState._(lastError: lastError);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
