import 'package:beets_player/core/sdk/app_error.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import 'error_bloc_state.dart';

class ErrorBloc extends Bloc<ErrorEvent, ErrorBlocState> {
  @override
  ErrorBlocState get initialState => ErrorBlocState();

  @override
  Stream<ErrorBlocState> mapEventToState(ErrorEvent event) async* {
    if (event is AppErrorEvent) {
      yield currentState.rebuild((b) => b..lastError = event);
    }
  }
}

abstract class ErrorEvent {}

@immutable
class AppErrorEvent extends ErrorEvent {
  final AppError error;
  final String source;

  AppErrorEvent(this.error, this.source);
}
