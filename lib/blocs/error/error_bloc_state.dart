import 'package:built_value/built_value.dart';

import 'error_bloc.dart';
part 'error_bloc_state.g.dart';

abstract class ErrorBlocState implements Built<ErrorBlocState, ErrorBlocStateBuilder> {
  @nullable
  AppErrorEvent get lastError;

  ErrorBlocState._();
  factory ErrorBlocState([void Function(ErrorBlocStateBuilder) updates]) = _$ErrorBlocState;
}
