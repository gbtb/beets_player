import 'package:beets_player/core/sdk/app_error.dart';
import 'package:beets_player/core/sdk/time/time_provider.dart';
import 'package:bloc/bloc.dart';

import 'error/error_bloc.dart';

abstract class BaseBloc<TEvent, TState> extends Bloc<TEvent, TState> {
  final ErrorBloc _errorBloc;
  final ITimeProvider _timeProvider;
  ITimeProvider get timeProvider => _timeProvider;

  void reportError(AppError err) {
    _errorBloc.dispatch(AppErrorEvent(err, blocRepresentation));
  }

  void reportInfo(String info) {
    print("$blocRepresentation | $info");
  }

  void reportDebug(String info) {
    print("DBG | $blocRepresentation | $info");
  }

  String get blocRepresentation;

  BaseBloc(this._errorBloc, this._timeProvider);
}
