import 'package:beets_player/blocs/base_bloc.dart';
import 'package:beets_player/blocs/error/error_bloc.dart';
import 'package:beets_player/blocs/player/player_bloc.dart';
import 'package:beets_player/blocs/playlist/playlist_bloc_state.dart';
import 'package:beets_player/blocs/playlist_scroll/playlist_scroll_bloc.dart' as scroll;
import 'package:beets_player/core/models/library_item.dart';
import 'package:beets_player/core/sdk/app_error.dart';
import 'package:beets_player/core/sdk/time/time_provider.dart';
import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';

///Represents a single playlist
class PlaylistBloc extends BaseBloc<PlaylistEvent, PlaylistBlocState> {
  final PlayerBloc _playerBloc;

  final String _name;

  final Iterable<LibraryItem> _sourceItems;

  scroll.PlaylistScrollBloc scrollBloc;

  PlaylistBloc(this._name, this._sourceItems, this._playerBloc, ErrorBloc _errorBloc, ITimeProvider timeProvider)
      : super(_errorBloc, timeProvider) {
    scrollBloc = scroll.PlaylistScrollBloc(ScrollController(), _errorBloc, timeProvider);
    scrollBloc.dispatch(scroll.Init()); //since initial state is not getting called, ought to do this bullshit
  }

  @override
  PlaylistBlocState get initialState => PlaylistBlocState((b) => b
    ..items.addAll(_sourceItems)
    ..name = _name);

  @override
  Stream<PlaylistBlocState> mapEventToState(PlaylistEvent event) async* {
    if (event is AddItemsToPlaylist) {
      yield this.currentState.rebuild((b) => b..items.addAll(event.items));
    }
    if (event is PlayThisSong) {
      _playerBloc.dispatch(SetPlaylistAndItem(this, event.item));
    }
    if (event is SetSelectedItem) {
      final idx = currentState.items.indexOf(event.item);
      if (idx >= 0) {
        yield this.currentState.rebuild((b) => b
          ..selectedItem.replace(event.item)
          ..selectedIndex = idx);

        scrollBloc.dispatch(scroll.SetSelectedItem(idx));
      } else
        reportError(ItemNotInExpectedCollection(event.item.toString(), "playlist"));
    } else if (event is MoveSelectionUp) {
      if (currentState.selectedIndex == null && currentState.items.length > 0) {
        yield currentState.rebuild((b) => b
          ..selectedItem.replace(b.items[0])
          ..selectedIndex = 0);
        scrollBloc.dispatch(scroll.SetSelectedItem(0));
      } else if (currentState.selectedIndex == 0) {
        yield currentState.rebuild((b) => b
          ..selectedItem.replace(b.items.last)
          ..selectedIndex = b.items.length - 1);
        scrollBloc.dispatch(scroll.SetSelectedItem(currentState.items.length - 1));
      } else {
        yield currentState.rebuild((b) => b
          ..selectedItem.replace(b.items[currentState.selectedIndex - 1])
          ..selectedIndex = currentState.selectedIndex - 1);
        scrollBloc.dispatch(scroll.MoveSelectionUp());
      }
    } else if (event is MoveSelectionDown) {
      if (currentState.selectedIndex == null && currentState.items.length > 0) {
        yield currentState.rebuild((b) => b
          ..selectedItem.replace(b.items[0])
          ..selectedIndex = 0);
        scrollBloc.dispatch(scroll.SetSelectedItem(0));
      } else if (currentState.selectedIndex == currentState.items.length) {
        yield currentState.rebuild((b) => b
          ..selectedItem.replace(b.items.first)
          ..selectedIndex = 0);
        scrollBloc.dispatch(scroll.SetSelectedItem(0));
      } else {
        yield currentState.rebuild((b) => b
          ..selectedItem.replace(b.items[currentState.selectedIndex + 1])
          ..selectedIndex = currentState.selectedIndex + 1);
        scrollBloc.dispatch(scroll.MoveSelectionDown());
      }
    }
  }

  ///Gets next item to play from playlist
  LibraryItem getNextItem(LibraryItem prevItem) {
    if (this.currentState.items.isEmpty) return null; //TODO: option type

    if (prevItem == null) return this.currentState.items.first;

    final idx = this.currentState.items.indexOf(prevItem);
    if (idx == -1 || idx == this.currentState.items.length - 1) return null;

    return this.currentState.items[idx + 1];
  }

  @override
  String get blocRepresentation => "Playlist $_name";
}

class PlaylistEvent {}

@immutable
class AddItemsToPlaylist implements PlaylistEvent {
  final Iterable<LibraryItem> items;

  AddItemsToPlaylist(this.items);
}

@immutable
class PlayThisSong implements PlaylistEvent {
  final LibraryItem item;

  PlayThisSong(this.item);
}

@immutable
class SetSelectedItem implements PlaylistEvent {
  final LibraryItem item;

  SetSelectedItem(this.item);
}

@immutable
class MoveSelectionUp implements PlaylistEvent {}

@immutable
class MoveSelectionDown implements PlaylistEvent {}
