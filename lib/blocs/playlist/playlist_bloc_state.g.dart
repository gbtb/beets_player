// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'playlist_bloc_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PlaylistBlocState extends PlaylistBlocState {
  @override
  final BuiltList<LibraryItem> items;
  @override
  final String name;
  @override
  final LibraryItem selectedItem;
  @override
  final int selectedIndex;
  Duration __totalDuration;

  factory _$PlaylistBlocState(
          [void Function(PlaylistBlocStateBuilder) updates]) =>
      (new PlaylistBlocStateBuilder()..update(updates)).build();

  _$PlaylistBlocState._(
      {this.items, this.name, this.selectedItem, this.selectedIndex})
      : super._() {
    if (items == null) {
      throw new BuiltValueNullFieldError('PlaylistBlocState', 'items');
    }
    if (name == null) {
      throw new BuiltValueNullFieldError('PlaylistBlocState', 'name');
    }
  }

  @override
  Duration get totalDuration => __totalDuration ??= super.totalDuration;

  @override
  PlaylistBlocState rebuild(void Function(PlaylistBlocStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PlaylistBlocStateBuilder toBuilder() =>
      new PlaylistBlocStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PlaylistBlocState &&
        items == other.items &&
        name == other.name &&
        selectedItem == other.selectedItem &&
        selectedIndex == other.selectedIndex;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, items.hashCode), name.hashCode), selectedItem.hashCode),
        selectedIndex.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PlaylistBlocState')
          ..add('items', items)
          ..add('name', name)
          ..add('selectedItem', selectedItem)
          ..add('selectedIndex', selectedIndex))
        .toString();
  }
}

class PlaylistBlocStateBuilder
    implements Builder<PlaylistBlocState, PlaylistBlocStateBuilder> {
  _$PlaylistBlocState _$v;

  ListBuilder<LibraryItem> _items;
  ListBuilder<LibraryItem> get items =>
      _$this._items ??= new ListBuilder<LibraryItem>();
  set items(ListBuilder<LibraryItem> items) => _$this._items = items;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  LibraryItemBuilder _selectedItem;
  LibraryItemBuilder get selectedItem =>
      _$this._selectedItem ??= new LibraryItemBuilder();
  set selectedItem(LibraryItemBuilder selectedItem) =>
      _$this._selectedItem = selectedItem;

  int _selectedIndex;
  int get selectedIndex => _$this._selectedIndex;
  set selectedIndex(int selectedIndex) => _$this._selectedIndex = selectedIndex;

  PlaylistBlocStateBuilder();

  PlaylistBlocStateBuilder get _$this {
    if (_$v != null) {
      _items = _$v.items?.toBuilder();
      _name = _$v.name;
      _selectedItem = _$v.selectedItem?.toBuilder();
      _selectedIndex = _$v.selectedIndex;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PlaylistBlocState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PlaylistBlocState;
  }

  @override
  void update(void Function(PlaylistBlocStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PlaylistBlocState build() {
    _$PlaylistBlocState _$result;
    try {
      _$result = _$v ??
          new _$PlaylistBlocState._(
              items: items.build(),
              name: name,
              selectedItem: _selectedItem?.build(),
              selectedIndex: selectedIndex);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'items';
        items.build();

        _$failedField = 'selectedItem';
        _selectedItem?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PlaylistBlocState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
