import 'package:beets_player/core/models/library_item.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
part 'playlist_bloc_state.g.dart';

abstract class PlaylistBlocState implements Built<PlaylistBlocState, PlaylistBlocStateBuilder> {
  BuiltList<LibraryItem> get items;
  String get name;
  @nullable
  LibraryItem get selectedItem;

  @nullable
  int get selectedIndex;

  @memoized
  Duration get totalDuration => items.fold(Duration.zero, (dur, item) => dur + item.duration);

  PlaylistBlocState._();
  factory PlaylistBlocState([void Function(PlaylistBlocStateBuilder) updates]) = _$PlaylistBlocState;
}
