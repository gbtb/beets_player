import 'package:beets_player/core/models/library_item.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';

part 'search_bloc_state.g.dart';

abstract class SearchBlocState implements Built<SearchBlocState, SearchBlocStateBuilder> {
  BuiltList<LibraryItem> get resultList;
  @nullable
  String get query;
  SearchBlocState._();
  factory SearchBlocState([void Function(SearchBlocStateBuilder) updates]) = _$SearchBlocState;
}
