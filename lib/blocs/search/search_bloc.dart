import 'package:beets_player/blocs/error/error_bloc.dart';
import 'package:beets_player/core/sdk/app_error.dart';
import 'package:beets_player/core/sdk/result.dart';
import 'package:beets_player/core/sdk/time/time_provider.dart';

import '../base_bloc.dart';
import './search_bloc_state.dart';
import 'package:beets_player/core/search_repository.dart';
import 'package:meta/meta.dart';

///Responsible for searching in beets library
class SearchBloc extends BaseBloc<SearchBlocEvent, SearchBlocState> {
  Result<AppError, ISearchRepository> _searchRepository;

  SearchBloc(ErrorBloc _errorBloc, ITimeProvider timeProvider) : super(_errorBloc, timeProvider);

  @override
  SearchBlocState get initialState => SearchBlocState();

  @override
  Stream<SearchBlocState> mapEventToState(SearchBlocEvent event) async* {
    if (event is SetRepository) {
      _searchRepository = event.repository;
      return;
    }

    if (_searchRepository == null || _searchRepository.isErr()) {
      reportError(BeetsDbNotFound());
      return;
    }
    if (event is PerformSimpleSearch) {
      final results = await _searchRepository.ok.simpleSearch(event.searchStr);

      if (results.isErr()) {
        reportError(results.err);
        return;
      }

      yield this.currentState.rebuild((b) => b
        ..resultList.replace(results.ok)
        ..query = event.searchStr);
    }
  }

  @override
  String get blocRepresentation => "Beets repo";
}

abstract class SearchBlocEvent {}

@immutable
class PerformSimpleSearch implements SearchBlocEvent {
  final String searchStr;

  PerformSimpleSearch(this.searchStr);
}

@immutable
class SetRepository implements SearchBlocEvent {
  final Result<AppError, ISearchRepository> repository;

  SetRepository(this.repository);
}

class BeetsDbNotFound extends AppError {
  @override
  String get message => "Beets database was not found. Please, set db file location in settings";
}
