// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_bloc_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SearchBlocState extends SearchBlocState {
  @override
  final BuiltList<LibraryItem> resultList;
  @override
  final String query;

  factory _$SearchBlocState([void Function(SearchBlocStateBuilder) updates]) =>
      (new SearchBlocStateBuilder()..update(updates)).build();

  _$SearchBlocState._({this.resultList, this.query}) : super._() {
    if (resultList == null) {
      throw new BuiltValueNullFieldError('SearchBlocState', 'resultList');
    }
  }

  @override
  SearchBlocState rebuild(void Function(SearchBlocStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SearchBlocStateBuilder toBuilder() =>
      new SearchBlocStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SearchBlocState &&
        resultList == other.resultList &&
        query == other.query;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, resultList.hashCode), query.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SearchBlocState')
          ..add('resultList', resultList)
          ..add('query', query))
        .toString();
  }
}

class SearchBlocStateBuilder
    implements Builder<SearchBlocState, SearchBlocStateBuilder> {
  _$SearchBlocState _$v;

  ListBuilder<LibraryItem> _resultList;
  ListBuilder<LibraryItem> get resultList =>
      _$this._resultList ??= new ListBuilder<LibraryItem>();
  set resultList(ListBuilder<LibraryItem> resultList) =>
      _$this._resultList = resultList;

  String _query;
  String get query => _$this._query;
  set query(String query) => _$this._query = query;

  SearchBlocStateBuilder();

  SearchBlocStateBuilder get _$this {
    if (_$v != null) {
      _resultList = _$v.resultList?.toBuilder();
      _query = _$v.query;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SearchBlocState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SearchBlocState;
  }

  @override
  void update(void Function(SearchBlocStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SearchBlocState build() {
    _$SearchBlocState _$result;
    try {
      _$result = _$v ??
          new _$SearchBlocState._(resultList: resultList.build(), query: query);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'resultList';
        resultList.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'SearchBlocState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
