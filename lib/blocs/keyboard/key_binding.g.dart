// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'key_binding.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$KeyBinding extends KeyBinding {
  @override
  final KeyAction action;
  @override
  final String description;
  @override
  final LogicalKeyboardKey key;

  factory _$KeyBinding([void Function(KeyBindingBuilder) updates]) =>
      (new KeyBindingBuilder()..update(updates)).build();

  _$KeyBinding._({this.action, this.description, this.key}) : super._() {
    if (action == null) {
      throw new BuiltValueNullFieldError('KeyBinding', 'action');
    }
    if (description == null) {
      throw new BuiltValueNullFieldError('KeyBinding', 'description');
    }
  }

  @override
  KeyBinding rebuild(void Function(KeyBindingBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  KeyBindingBuilder toBuilder() => new KeyBindingBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is KeyBinding &&
        action == other.action &&
        description == other.description &&
        key == other.key;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, action.hashCode), description.hashCode), key.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('KeyBinding')
          ..add('action', action)
          ..add('description', description)
          ..add('key', key))
        .toString();
  }
}

class KeyBindingBuilder implements Builder<KeyBinding, KeyBindingBuilder> {
  _$KeyBinding _$v;

  KeyAction _action;
  KeyAction get action => _$this._action;
  set action(KeyAction action) => _$this._action = action;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  LogicalKeyboardKey _key;
  LogicalKeyboardKey get key => _$this._key;
  set key(LogicalKeyboardKey key) => _$this._key = key;

  KeyBindingBuilder();

  KeyBindingBuilder get _$this {
    if (_$v != null) {
      _action = _$v.action;
      _description = _$v.description;
      _key = _$v.key;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(KeyBinding other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$KeyBinding;
  }

  @override
  void update(void Function(KeyBindingBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$KeyBinding build() {
    final _$result = _$v ??
        new _$KeyBinding._(action: action, description: description, key: key);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
