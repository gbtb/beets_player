import 'dart:async';

import 'package:beets_player/blocs/error/error_bloc.dart';
import 'package:beets_player/blocs/keyboard/key_action.dart';
import 'package:beets_player/core/sdk/app_error.dart';
import 'package:beets_player/core/sdk/time/time_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:meta/meta.dart';

import '../base_bloc.dart';
import 'key_binding.dart';
import 'keyboard_bloc_state.dart';

class KeyboardBloc extends BaseBloc<KeyboardBlocEvent, KeyboardBlocState> {
  KeyboardBloc(ErrorBloc errorBloc, ITimeProvider timeProvider) : super(errorBloc, timeProvider);

  @override
  KeyboardBlocState get initialState => KeyboardBlocState((b) => b.bindings = Map.fromIterable(<KeyBinding>[
        KeyBinding((b) => b
          ..action = KeyAction.Play
          ..description = "Play/Pause playback"
          ..key = LogicalKeyboardKey.space)
      ], key: (item) => item.key));

  @override
  Stream<KeyboardBlocState> mapEventToState(
    event,
  ) async* {}

  @override
  String get blocRepresentation => "Keyboard bindings";
}

@immutable
abstract class KeyboardBlocEvent {}

class WrongKeyEventDataError extends AppError {
  String _runtimeType;

  WrongKeyEventDataError(RawKeyEventData data) {
    _runtimeType = data.runtimeType.toString();
  }

  @override
  String get message => "Unsupported key data type: $_runtimeType";
}
