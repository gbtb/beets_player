import 'package:built_value/built_value.dart';
import 'package:flutter/services.dart';

import 'key_action.dart';
part 'key_binding.g.dart';

abstract class KeyBinding implements Built<KeyBinding, KeyBindingBuilder> {
  KeyAction get action;
  String get description;
  //Action handler;

  @nullable
  LogicalKeyboardKey get key;

  KeyBinding._();
  factory KeyBinding([void Function(KeyBindingBuilder) updates]) = _$KeyBinding;
}
