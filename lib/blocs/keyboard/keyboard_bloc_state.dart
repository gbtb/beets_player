import 'package:beets_player/blocs/keyboard/key_action.dart';
import 'package:beets_player/blocs/keyboard/keyboard_bloc.dart';
import 'package:built_value/built_value.dart';
import 'package:flutter/services.dart';

import 'key_binding.dart';

part 'keyboard_bloc_state.g.dart';

abstract class KeyboardBlocState implements Built<KeyboardBlocState, KeyboardBlocStateBuilder> {
  Map<LogicalKeyboardKey, KeyBinding> get bindings;

  KeyboardBlocState._();
  factory KeyboardBlocState([void Function(KeyboardBlocStateBuilder) updates]) = _$KeyboardBlocState;
}
