import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
part 'key_action.g.dart';

class KeyAction extends EnumClass {
  @BuiltValueEnumConst(fallback: true)
  static const KeyAction Unknown = _$unknown;
  static const KeyAction Play = _$play;

  const KeyAction._(String name) : super(name);

  static BuiltSet<KeyAction> get values => _$values;
  static KeyAction valueOf(String name) => _$valueOf(name);
}
