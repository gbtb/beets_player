// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'key_action.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const KeyAction _$unknown = const KeyAction._('Unknown');
const KeyAction _$play = const KeyAction._('Play');

KeyAction _$valueOf(String name) {
  switch (name) {
    case 'Unknown':
      return _$unknown;
    case 'Play':
      return _$play;
    default:
      return _$unknown;
  }
}

final BuiltSet<KeyAction> _$values = new BuiltSet<KeyAction>(const <KeyAction>[
  _$unknown,
  _$play,
]);

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
