// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'keyboard_bloc_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$KeyboardBlocState extends KeyboardBlocState {
  @override
  final Map<LogicalKeyboardKey, KeyBinding> bindings;

  factory _$KeyboardBlocState(
          [void Function(KeyboardBlocStateBuilder) updates]) =>
      (new KeyboardBlocStateBuilder()..update(updates)).build();

  _$KeyboardBlocState._({this.bindings}) : super._() {
    if (bindings == null) {
      throw new BuiltValueNullFieldError('KeyboardBlocState', 'bindings');
    }
  }

  @override
  KeyboardBlocState rebuild(void Function(KeyboardBlocStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  KeyboardBlocStateBuilder toBuilder() =>
      new KeyboardBlocStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is KeyboardBlocState && bindings == other.bindings;
  }

  @override
  int get hashCode {
    return $jf($jc(0, bindings.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('KeyboardBlocState')
          ..add('bindings', bindings))
        .toString();
  }
}

class KeyboardBlocStateBuilder
    implements Builder<KeyboardBlocState, KeyboardBlocStateBuilder> {
  _$KeyboardBlocState _$v;

  Map<LogicalKeyboardKey, KeyBinding> _bindings;
  Map<LogicalKeyboardKey, KeyBinding> get bindings => _$this._bindings;
  set bindings(Map<LogicalKeyboardKey, KeyBinding> bindings) =>
      _$this._bindings = bindings;

  KeyboardBlocStateBuilder();

  KeyboardBlocStateBuilder get _$this {
    if (_$v != null) {
      _bindings = _$v.bindings;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(KeyboardBlocState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$KeyboardBlocState;
  }

  @override
  void update(void Function(KeyboardBlocStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$KeyboardBlocState build() {
    final _$result = _$v ?? new _$KeyboardBlocState._(bindings: bindings);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
