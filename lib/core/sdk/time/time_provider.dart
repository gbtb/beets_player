import 'dart:async';

import 'modified_stopwatch.dart';

abstract class ITimeProvider {
  Timer periodic(Duration duration, void Function(Timer) callback);
  Stopwatch getStopwatch();

  Duration adjustDuration(Duration d);
}

class TimeProvider implements ITimeProvider {
  final double _timeMult;

  TimeProvider(this._timeMult);

  @override
  Stopwatch getStopwatch() {
    return ModifiedStopwatch(_timeMult);
  }

  @override
  Timer periodic(Duration duration, void Function(Timer) callback) {
    return Timer.periodic(adjustDuration(duration), callback);
  }

  @override
  Duration adjustDuration(Duration d) {
    return d * _timeMult;
  }
}

class RealTimeProvider implements ITimeProvider {
  @override
  Duration adjustDuration(Duration d) {
    return d;
  }

  @override
  Stopwatch getStopwatch() {
    return Stopwatch();
  }

  @override
  Timer periodic(Duration duration, void Function(Timer) callback) {
    return Timer.periodic(duration, callback);
  }
}
