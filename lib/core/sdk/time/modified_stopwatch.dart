import 'duration.dart';

class ModifiedStopwatch extends Stopwatch {
  final double _timeMult;

  ModifiedStopwatch(this._timeMult);

  Duration get elapsed => super.elapsed * (1 / _timeMult);
}
