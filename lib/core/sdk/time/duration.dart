class DurationFormatter {
  static String format(Duration d) {
    final buffer = new StringBuffer();

    if (d.inHours > 0) {
      buffer.write("${d.inHours}".padLeft(2, "0"));
      buffer.write(":");
    }

    buffer.write("${d.inMinutes % 60}".padLeft(2, "0"));
    buffer.write(":");
    buffer.write("${d.inSeconds % 60}".padLeft(2, "0"));

    return buffer.toString();
  }
}
