import 'package:beets_player/core/playback_service.dart';

///Base class for Application Errors - errors which are expected in domain
abstract class AppError {
  ///Short, one sentence message
  String get message;

  ///Detailed explanation
  String get description => message;
}

class CaughtError extends AppError implements PlaybackError {
  final Error exception;

  CaughtError(this.exception);

  @override
  String get message => exception.toString();
}

class CaughtException extends AppError implements PlaybackError {
  final Exception exception;

  CaughtException(this.exception);

  @override
  String get message => exception.toString();
}

class ItemNotInExpectedCollection extends AppError {
  final String _item;
  final String _collection;

  ItemNotInExpectedCollection(this._item, this._collection);

  @override
  String get message => "Item $_item was not found in collection $_collection";
}
