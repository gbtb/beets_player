import 'package:meta/meta.dart';

///Represents results of methods, which can ended up with errors.
///See Haskell's Either or Elm's Result types
@immutable
class Result<ErrT, OkT> {
  ErrT _err;
  OkT _ok;

  ErrT get err => _err;
  OkT get ok => _ok;

  Result.fromOk(OkT val) {
    this._ok = val;
  }

  Result.fromErr(ErrT val) {
    this._err = val;
  }

  bool isOk() {
    return _ok != null;
  }

  bool isErr() {
    return _err != null;
  }

  Future<Result<ErrT, NewOkT>> map<NewOkT>(Future<NewOkT> Function(OkT) func) async {
    if (isOk()) return Result.fromOk(await func(_ok));

    return Result.fromErr(_err);
  }

  Future<Result<ErrT, NewOkT>> andThen<NewOkT>(Future<Result<ErrT, NewOkT>> Function(OkT) func) async {
    if (isOk()) return await func(_ok);

    return Result.fromErr(_err);
  }
}
