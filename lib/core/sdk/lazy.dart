class Lazy<T> {
  T _value;

  T Function() initializer;

  Lazy(this.initializer);

  T get value {
    if (_value == null) {
      _value = initializer();
    }

    return _value;
  }
}
