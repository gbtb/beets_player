import 'package:beets_player/core/sdk/app_error.dart';
import 'package:beets_player/core/sdk/result.dart';
import 'package:built_collection/built_collection.dart';

import 'models/library_item.dart';

abstract class ISearchRepository {
  /// Performs simple searh of query string in common lib item's fields - title, album, artist
  Future<Result<AppError, BuiltList<LibraryItem>>> simpleSearch(String query);

  ///
  Future<Result<AppError, ISearchRepository>> init(Uri pathToDatabase);
}
