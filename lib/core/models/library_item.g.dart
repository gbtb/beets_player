// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'library_item.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<LibraryItem> _$libraryItemSerializer = new _$LibraryItemSerializer();

class _$LibraryItemSerializer implements StructuredSerializer<LibraryItem> {
  @override
  final Iterable<Type> types = const [LibraryItem, _$LibraryItem];
  @override
  final String wireName = 'LibraryItem';

  @override
  Iterable<Object> serialize(Serializers serializers, LibraryItem object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'artist',
      serializers.serialize(object.artist,
          specifiedType: const FullType(String)),
      'album',
      serializers.serialize(object.album,
          specifiedType: const FullType(String)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
      'duration',
      serializers.serialize(object.duration,
          specifiedType: const FullType(Duration)),
    ];
    if (object.location != null) {
      result
        ..add('location')
        ..add(serializers.serialize(object.location,
            specifiedType: const FullType(Uri)));
    }
    return result;
  }

  @override
  LibraryItem deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LibraryItemBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'artist':
          result.artist = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'album':
          result.album = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'duration':
          result.duration = serializers.deserialize(value,
              specifiedType: const FullType(Duration)) as Duration;
          break;
        case 'location':
          result.location = serializers.deserialize(value,
              specifiedType: const FullType(Uri)) as Uri;
          break;
      }
    }

    return result.build();
  }
}

class _$LibraryItem extends LibraryItem {
  @override
  final String artist;
  @override
  final String album;
  @override
  final String title;
  @override
  final Duration duration;
  @override
  final Uri location;

  factory _$LibraryItem([void Function(LibraryItemBuilder) updates]) =>
      (new LibraryItemBuilder()..update(updates)).build();

  _$LibraryItem._(
      {this.artist, this.album, this.title, this.duration, this.location})
      : super._() {
    if (artist == null) {
      throw new BuiltValueNullFieldError('LibraryItem', 'artist');
    }
    if (album == null) {
      throw new BuiltValueNullFieldError('LibraryItem', 'album');
    }
    if (title == null) {
      throw new BuiltValueNullFieldError('LibraryItem', 'title');
    }
    if (duration == null) {
      throw new BuiltValueNullFieldError('LibraryItem', 'duration');
    }
  }

  @override
  LibraryItem rebuild(void Function(LibraryItemBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LibraryItemBuilder toBuilder() => new LibraryItemBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LibraryItem &&
        artist == other.artist &&
        album == other.album &&
        title == other.title &&
        duration == other.duration &&
        location == other.location;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, artist.hashCode), album.hashCode), title.hashCode),
            duration.hashCode),
        location.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('LibraryItem')
          ..add('artist', artist)
          ..add('album', album)
          ..add('title', title)
          ..add('duration', duration)
          ..add('location', location))
        .toString();
  }
}

class LibraryItemBuilder implements Builder<LibraryItem, LibraryItemBuilder> {
  _$LibraryItem _$v;

  String _artist;
  String get artist => _$this._artist;
  set artist(String artist) => _$this._artist = artist;

  String _album;
  String get album => _$this._album;
  set album(String album) => _$this._album = album;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  Duration _duration;
  Duration get duration => _$this._duration;
  set duration(Duration duration) => _$this._duration = duration;

  Uri _location;
  Uri get location => _$this._location;
  set location(Uri location) => _$this._location = location;

  LibraryItemBuilder();

  LibraryItemBuilder get _$this {
    if (_$v != null) {
      _artist = _$v.artist;
      _album = _$v.album;
      _title = _$v.title;
      _duration = _$v.duration;
      _location = _$v.location;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LibraryItem other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$LibraryItem;
  }

  @override
  void update(void Function(LibraryItemBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$LibraryItem build() {
    final _$result = _$v ??
        new _$LibraryItem._(
            artist: artist,
            album: album,
            title: title,
            duration: duration,
            location: location);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
