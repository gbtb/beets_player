// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'volume_level.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$VolumeLevel extends VolumeLevel {
  @override
  final double volume;

  factory _$VolumeLevel([void Function(VolumeLevelBuilder) updates]) =>
      (new VolumeLevelBuilder()..update(updates)).build() as _$VolumeLevel;

  _$VolumeLevel._({this.volume}) : super._() {
    if (volume == null) {
      throw new BuiltValueNullFieldError('VolumeLevel', 'volume');
    }
  }

  @override
  VolumeLevel rebuild(void Function(VolumeLevelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  _$VolumeLevelBuilder toBuilder() => new _$VolumeLevelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is VolumeLevel && volume == other.volume;
  }

  @override
  int get hashCode {
    return $jf($jc(0, volume.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('VolumeLevel')..add('volume', volume))
        .toString();
  }
}

class _$VolumeLevelBuilder extends VolumeLevelBuilder {
  _$VolumeLevel _$v;

  @override
  double get volume {
    _$this;
    return super.volume;
  }

  @override
  set volume(double volume) {
    _$this;
    super.volume = volume;
  }

  _$VolumeLevelBuilder() : super._();

  VolumeLevelBuilder get _$this {
    if (_$v != null) {
      super.volume = _$v.volume;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(VolumeLevel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$VolumeLevel;
  }

  @override
  void update(void Function(VolumeLevelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$VolumeLevel build() {
    final _$result = _$v ?? new _$VolumeLevel._(volume: volume);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
