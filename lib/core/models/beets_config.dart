import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
part 'beets_config.g.dart';

abstract class BeetsConfig implements Built<BeetsConfig, BeetsConfigBuilder> {
  Uri get library;
  Uri get directory;

  BeetsConfig._();
  factory BeetsConfig([void Function(BeetsConfigBuilder) updates]) = _$BeetsConfig;

  static Serializer<BeetsConfig> get serializer => _$beetsConfigSerializer;
}
