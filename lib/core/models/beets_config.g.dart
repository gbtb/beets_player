// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'beets_config.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<BeetsConfig> _$beetsConfigSerializer = new _$BeetsConfigSerializer();

class _$BeetsConfigSerializer implements StructuredSerializer<BeetsConfig> {
  @override
  final Iterable<Type> types = const [BeetsConfig, _$BeetsConfig];
  @override
  final String wireName = 'BeetsConfig';

  @override
  Iterable<Object> serialize(Serializers serializers, BeetsConfig object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'library',
      serializers.serialize(object.library, specifiedType: const FullType(Uri)),
      'directory',
      serializers.serialize(object.directory,
          specifiedType: const FullType(Uri)),
    ];

    return result;
  }

  @override
  BeetsConfig deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BeetsConfigBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'library':
          result.library = serializers.deserialize(value,
              specifiedType: const FullType(Uri)) as Uri;
          break;
        case 'directory':
          result.directory = serializers.deserialize(value,
              specifiedType: const FullType(Uri)) as Uri;
          break;
      }
    }

    return result.build();
  }
}

class _$BeetsConfig extends BeetsConfig {
  @override
  final Uri library;
  @override
  final Uri directory;

  factory _$BeetsConfig([void Function(BeetsConfigBuilder) updates]) =>
      (new BeetsConfigBuilder()..update(updates)).build();

  _$BeetsConfig._({this.library, this.directory}) : super._() {
    if (library == null) {
      throw new BuiltValueNullFieldError('BeetsConfig', 'library');
    }
    if (directory == null) {
      throw new BuiltValueNullFieldError('BeetsConfig', 'directory');
    }
  }

  @override
  BeetsConfig rebuild(void Function(BeetsConfigBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BeetsConfigBuilder toBuilder() => new BeetsConfigBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BeetsConfig &&
        library == other.library &&
        directory == other.directory;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, library.hashCode), directory.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('BeetsConfig')
          ..add('library', library)
          ..add('directory', directory))
        .toString();
  }
}

class BeetsConfigBuilder implements Builder<BeetsConfig, BeetsConfigBuilder> {
  _$BeetsConfig _$v;

  Uri _library;
  Uri get library => _$this._library;
  set library(Uri library) => _$this._library = library;

  Uri _directory;
  Uri get directory => _$this._directory;
  set directory(Uri directory) => _$this._directory = directory;

  BeetsConfigBuilder();

  BeetsConfigBuilder get _$this {
    if (_$v != null) {
      _library = _$v.library;
      _directory = _$v.directory;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BeetsConfig other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$BeetsConfig;
  }

  @override
  void update(void Function(BeetsConfigBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$BeetsConfig build() {
    final _$result =
        _$v ?? new _$BeetsConfig._(library: library, directory: directory);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
