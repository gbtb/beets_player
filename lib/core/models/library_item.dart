import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'library_item.g.dart';

abstract class LibraryItem implements Built<LibraryItem, LibraryItemBuilder> {
  String get artist;
  String get album;
  String get title;
  Duration get duration;

  @nullable
  Uri get location;

  LibraryItem._();
  factory LibraryItem([void Function(LibraryItemBuilder) updates]) = _$LibraryItem;

  static Serializer<LibraryItem> get serializer => _$libraryItemSerializer;
}
