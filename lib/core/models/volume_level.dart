import 'package:built_value/built_value.dart';

part 'volume_level.g.dart';

/// todo: @unsafe attr
abstract class VolumeLevel implements Built<VolumeLevel, VolumeLevelBuilder> {
  double get volume;

  VolumeLevel._();
  factory VolumeLevel([void Function(VolumeLevelBuilder) updates]) => _$VolumeLevel(updates);
}

abstract class VolumeLevelBuilder implements Builder<VolumeLevel, VolumeLevelBuilder> {
  _$VolumeLevel _$v;

  double _volume;
  double get volume => _$this._volume;
  set volume(double volume) {
    if (volume >= 0 && volume <= 100) {
      _$this._volume = volume;
    } else {
      throw ArgumentError("Volume level must be in a range from 0 to 100");
    }
  }

  VolumeLevelBuilder._();

  factory VolumeLevelBuilder() = _$VolumeLevelBuilder;

  VolumeLevelBuilder get _$this {
    if (_$v != null) {
      _volume = _$v.volume;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(VolumeLevel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$VolumeLevel;
  }

  @override
  void update(void Function(VolumeLevelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$VolumeLevel build() {
    final _$result = _$v ?? new _$VolumeLevel._(volume: volume);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
