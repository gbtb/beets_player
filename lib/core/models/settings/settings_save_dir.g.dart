// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'settings_save_dir.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const SettingsSaveDir _$saveAlongsideDb =
    const SettingsSaveDir._('SaveAlongsideDb');
const SettingsSaveDir _$saveInsideLibrary =
    const SettingsSaveDir._('SaveInsideLibrary');
const SettingsSaveDir _$saveInCustomLocation =
    const SettingsSaveDir._('SaveInCustomLocation');

SettingsSaveDir _$valueOf(String name) {
  switch (name) {
    case 'SaveAlongsideDb':
      return _$saveAlongsideDb;
    case 'SaveInsideLibrary':
      return _$saveInsideLibrary;
    case 'SaveInCustomLocation':
      return _$saveInCustomLocation;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<SettingsSaveDir> _$values =
    new BuiltSet<SettingsSaveDir>(const <SettingsSaveDir>[
  _$saveAlongsideDb,
  _$saveInsideLibrary,
  _$saveInCustomLocation,
]);

Serializer<SettingsSaveDir> _$settingsSaveDirSerializer =
    new _$SettingsSaveDirSerializer();

class _$SettingsSaveDirSerializer
    implements PrimitiveSerializer<SettingsSaveDir> {
  @override
  final Iterable<Type> types = const <Type>[SettingsSaveDir];
  @override
  final String wireName = 'SettingsSaveDir';

  @override
  Object serialize(Serializers serializers, SettingsSaveDir object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  SettingsSaveDir deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      SettingsSaveDir.valueOf(serialized as String);
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
