// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'general_settings.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GeneralSettings> _$generalSettingsSerializer =
    new _$GeneralSettingsSerializer();

class _$GeneralSettingsSerializer
    implements StructuredSerializer<GeneralSettings> {
  @override
  final Iterable<Type> types = const [GeneralSettings, _$GeneralSettings];
  @override
  final String wireName = 'GeneralSettings';

  @override
  Iterable<Object> serialize(Serializers serializers, GeneralSettings object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'settingsSaveDir',
      serializers.serialize(object.settingsSaveDir,
          specifiedType: const FullType(SettingsSaveDir)),
    ];
    if (object.beetsDatabaseUri != null) {
      result
        ..add('beetsDatabaseUri')
        ..add(serializers.serialize(object.beetsDatabaseUri,
            specifiedType: const FullType(Uri)));
    }
    if (object.beetsLibraryDir != null) {
      result
        ..add('beetsLibraryDir')
        ..add(serializers.serialize(object.beetsLibraryDir,
            specifiedType: const FullType(Uri)));
    }
    if (object.settingsSaveUri != null) {
      result
        ..add('settingsSaveUri')
        ..add(serializers.serialize(object.settingsSaveUri,
            specifiedType: const FullType(Uri)));
    }
    return result;
  }

  @override
  GeneralSettings deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GeneralSettingsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'beetsDatabaseUri':
          result.beetsDatabaseUri = serializers.deserialize(value,
              specifiedType: const FullType(Uri)) as Uri;
          break;
        case 'beetsLibraryDir':
          result.beetsLibraryDir = serializers.deserialize(value,
              specifiedType: const FullType(Uri)) as Uri;
          break;
        case 'settingsSaveDir':
          result.settingsSaveDir = serializers.deserialize(value,
                  specifiedType: const FullType(SettingsSaveDir))
              as SettingsSaveDir;
          break;
        case 'settingsSaveUri':
          result.settingsSaveUri = serializers.deserialize(value,
              specifiedType: const FullType(Uri)) as Uri;
          break;
      }
    }

    return result.build();
  }
}

class _$GeneralSettings extends GeneralSettings {
  @override
  final Uri beetsDatabaseUri;
  @override
  final Uri beetsLibraryDir;
  @override
  final SettingsSaveDir settingsSaveDir;
  @override
  final Uri settingsSaveUri;

  factory _$GeneralSettings([void Function(GeneralSettingsBuilder) updates]) =>
      (new GeneralSettingsBuilder()..update(updates)).build();

  _$GeneralSettings._(
      {this.beetsDatabaseUri,
      this.beetsLibraryDir,
      this.settingsSaveDir,
      this.settingsSaveUri})
      : super._() {
    if (settingsSaveDir == null) {
      throw new BuiltValueNullFieldError('GeneralSettings', 'settingsSaveDir');
    }
  }

  @override
  GeneralSettings rebuild(void Function(GeneralSettingsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GeneralSettingsBuilder toBuilder() =>
      new GeneralSettingsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GeneralSettings &&
        beetsDatabaseUri == other.beetsDatabaseUri &&
        beetsLibraryDir == other.beetsLibraryDir &&
        settingsSaveDir == other.settingsSaveDir &&
        settingsSaveUri == other.settingsSaveUri;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, beetsDatabaseUri.hashCode), beetsLibraryDir.hashCode),
            settingsSaveDir.hashCode),
        settingsSaveUri.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GeneralSettings')
          ..add('beetsDatabaseUri', beetsDatabaseUri)
          ..add('beetsLibraryDir', beetsLibraryDir)
          ..add('settingsSaveDir', settingsSaveDir)
          ..add('settingsSaveUri', settingsSaveUri))
        .toString();
  }
}

class GeneralSettingsBuilder
    implements Builder<GeneralSettings, GeneralSettingsBuilder> {
  _$GeneralSettings _$v;

  Uri _beetsDatabaseUri;
  Uri get beetsDatabaseUri => _$this._beetsDatabaseUri;
  set beetsDatabaseUri(Uri beetsDatabaseUri) =>
      _$this._beetsDatabaseUri = beetsDatabaseUri;

  Uri _beetsLibraryDir;
  Uri get beetsLibraryDir => _$this._beetsLibraryDir;
  set beetsLibraryDir(Uri beetsLibraryDir) =>
      _$this._beetsLibraryDir = beetsLibraryDir;

  SettingsSaveDir _settingsSaveDir;
  SettingsSaveDir get settingsSaveDir => _$this._settingsSaveDir;
  set settingsSaveDir(SettingsSaveDir settingsSaveDir) =>
      _$this._settingsSaveDir = settingsSaveDir;

  Uri _settingsSaveUri;
  Uri get settingsSaveUri => _$this._settingsSaveUri;
  set settingsSaveUri(Uri settingsSaveUri) =>
      _$this._settingsSaveUri = settingsSaveUri;

  GeneralSettingsBuilder();

  GeneralSettingsBuilder get _$this {
    if (_$v != null) {
      _beetsDatabaseUri = _$v.beetsDatabaseUri;
      _beetsLibraryDir = _$v.beetsLibraryDir;
      _settingsSaveDir = _$v.settingsSaveDir;
      _settingsSaveUri = _$v.settingsSaveUri;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GeneralSettings other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GeneralSettings;
  }

  @override
  void update(void Function(GeneralSettingsBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GeneralSettings build() {
    final _$result = _$v ??
        new _$GeneralSettings._(
            beetsDatabaseUri: beetsDatabaseUri,
            beetsLibraryDir: beetsLibraryDir,
            settingsSaveDir: settingsSaveDir,
            settingsSaveUri: settingsSaveUri);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
