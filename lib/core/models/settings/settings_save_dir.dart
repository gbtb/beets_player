import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
part 'settings_save_dir.g.dart';

class SettingsSaveDir extends EnumClass {
  static const SettingsSaveDir SaveAlongsideDb = _$saveAlongsideDb;
  static const SettingsSaveDir SaveInsideLibrary = _$saveInsideLibrary;
  static const SettingsSaveDir SaveInCustomLocation = _$saveInCustomLocation;
  const SettingsSaveDir._(String name) : super(name);
  static BuiltSet<SettingsSaveDir> get values => _$values;
  static SettingsSaveDir valueOf(String name) => _$valueOf(name);

  String toHumanString() {
    if (this == SaveAlongsideDb) {
      return "With beets database";
    } else if (this == SaveInsideLibrary) {
      return "With beets library";
    } else {
      return "Other directory";
    }
  }

  static Serializer<SettingsSaveDir> get serializer => _$settingsSaveDirSerializer;
}
