import 'package:beets_player/core/beets_config_repository.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:logging/logging.dart';

import 'settings_save_dir.dart';

part 'general_settings.g.dart';

abstract class GeneralSettings implements Built<GeneralSettings, GeneralSettingsBuilder> {
  @nullable
  Uri get beetsDatabaseUri;

  @nullable
  Uri get beetsLibraryDir;

  SettingsSaveDir get settingsSaveDir;

  @nullable
  Uri get settingsSaveUri;

  static Future<GeneralSettings> init(IBeetsConfigRepository configRepository) async {
    final config = await configRepository.findConfig();
    if (config.isOk()) {
      Logger.root.info("Initialized general settings from beets config");
      return GeneralSettings((b) => b
        ..beetsDatabaseUri = config.ok.library
        ..beetsLibraryDir = config.ok.directory
        ..settingsSaveDir = SettingsSaveDir.SaveInsideLibrary);
    }

    Logger.root.warning("Failed to initialize general settings from beets config");
    return GeneralSettings((b) => b..settingsSaveDir = SettingsSaveDir.SaveInsideLibrary);
  }

  GeneralSettings._();
  factory GeneralSettings([void Function(GeneralSettingsBuilder) updates]) = _$GeneralSettings;

  static Serializer<GeneralSettings> get serializer => _$generalSettingsSerializer;
}
