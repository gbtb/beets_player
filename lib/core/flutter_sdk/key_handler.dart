import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

typedef BoolKeyHandlerCallback = bool Function(FocusNode);
typedef VoidKeyHandlerCallback = void Function(FocusNode);

/// Helper class for producing callbacks to handle specific key being pressed
abstract class AbstractKeyHandler {
  FocusOnKeyCallback get callback;
}

/// Handler which produces boolean callbacks
/// If callback returns true - key event wont float up in focus tree
class BoolKeyHandler implements AbstractKeyHandler {
  LogicalKeyboardKey _key;
  BoolKeyHandlerCallback _handler;

  /// Uses [_handler] to handle [_key] being pressed
  BoolKeyHandler(this._key, this._handler);

  FocusOnKeyCallback get callback => (focusNode, event) {
        if (event is RawKeyDownEvent) return true;

        if (event.logicalKey == _key) return _handler(focusNode);

        return false;
      };
}

/// Handler which consumes void callbacks, and event as well
class VoidKeyHandler implements AbstractKeyHandler {
  LogicalKeyboardKey _key;
  VoidKeyHandlerCallback _handler;

  VoidKeyHandler(this._key, this._handler);

  FocusOnKeyCallback get callback => (focusNode, event) {
        if (event is RawKeyDownEvent) return true;

        if (event.logicalKey == _key) {
          _handler(focusNode);
          return true;
        }

        return false;
      };
}

class KeyHandlerComposition implements AbstractKeyHandler {
  Iterable<AbstractKeyHandler> _handlers;

  KeyHandlerComposition(this._handlers);

  @override
  get callback => (focusNode, keyEvent) {
        for (var handler in _handlers) {
          if (handler.callback(focusNode, keyEvent)) return true;
        }

        return false;
      };
}
