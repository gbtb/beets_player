import 'package:beets_player/core/models/settings/general_settings.dart';
import 'package:beets_player/core/sdk/app_error.dart';
import 'package:beets_player/core/sdk/result.dart';

abstract class ISettingsRepository {
  Future<Result<AppError, GeneralSettings>> getGeneralSettings();
}

class SettingsNotFoundError extends AppError {
  @override
  String get message => "Beets player settings was not found in storage";
}
