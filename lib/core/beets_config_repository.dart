import 'package:beets_player/core/sdk/app_error.dart';
import 'package:beets_player/core/sdk/result.dart';

import 'models/beets_config.dart';

abstract class IBeetsConfigRepository {
  /// Tries to find beets config.yaml in common places
  Future<Result<AppError, BeetsConfig>> findConfig();

  /// Tries to read beets config.yaml from specific file
  Future<Result<AppError, BeetsConfig>> getConfig(Uri path);
}
