import 'package:beets_player/core/models/library_item.dart';
import 'package:beets_player/core/sdk/app_error.dart';
import 'package:beets_player/core/sdk/result.dart';

import 'models/volume_level.dart';

///Interface for music playback
abstract class IPlaybackService {
  Future<Result<PlaybackError, void>> setItem(LibraryItem item);
  Future<Result<PlaybackError, void>> play();
  Future<Result<PlaybackError, void>> pause();
  Future<Result<PlaybackError, void>> stop();
  Future<Result<PlaybackError, void>> setVolume(VolumeLevel volume);
  Future<Result<PlaybackError, void>> setMuted(bool isMuted);
}

abstract class PlaybackError extends AppError {}

class ItemNotFoundError extends PlaybackError {
  @override
  String get message => "File not found";
}
