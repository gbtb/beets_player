import 'package:ansicolor/ansicolor.dart';
import 'package:beets_player/ui/main_page/main_page.dart';
import 'package:beets_player/ui/settings_page/settings_page.dart';
import 'package:beets_player/ui/shared/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show debugDefaultTargetPlatformOverride;
import 'package:logging/logging.dart';

void main() {
  // Override is necessary to prevent Unknown platform' flutter startup error.
  debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;

  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((LogRecord rec) {
    final pen = getColorForTerminal(rec.level);
    print(pen('[${rec.time}] : [${rec.level.name}] : ${rec.message}'));
  });

  runApp(App());
}

AnsiPen getColorForTerminal(Level level) {
  if (level == Level.WARNING) return AnsiPen()..yellow();

  if (level == Level.SEVERE) return AnsiPen()..red();

  if (level == Level.INFO) return AnsiPen()..green();

  return AnsiPen()..reset();
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MainPage(),
      routes: {
        NavigationRoute.Unknown.toString(): (context) => MainPage(),
        NavigationRoute.Player.toString(): (context) => MainPage(),
        NavigationRoute.Settings.toString(): (context) => SettingsPage(),
      },
    );
  }
}
