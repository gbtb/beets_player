import 'package:beets_player/blocs/bloc_factory.dart';
import 'package:beets_player/blocs/settings/settings_bloc.dart';
import 'package:beets_player/blocs/settings/settings_bloc_state.dart';
import 'package:beets_player/ui/settings_page/settings_section.dart';
import 'package:beets_player/ui/shared/widgets/custom_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

import 'general_region.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  SettingsSection _selectedSection = SettingsSection.General;
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Beets Player"),
      ),
      drawer: CustomDrawer(key: CustomDrawer.thisKey),
      body: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Material(
              elevation: 5,
              type: MaterialType.canvas,
              child: ListView(
                children: <Widget>[
                  ListTile(
                    title: Text("General"),
                    onTap: () => setState(() {
                      _selectedSection = SettingsSection.General;
                    }),
                    selected: _selectedSection == SettingsSection.General,
                  ),
                  ListTile(
                    title: Text("Key bindings"),
                    onTap: () => setState(() {
                      _selectedSection = SettingsSection.KeyBindings;
                    }),
                    selected: _selectedSection == SettingsSection.KeyBindings,
                  ),
                ],
              ),
            ),
          ),
          BlocProvider(
            child: Expanded(
                flex: 4,
                child: BlocBuilder<SettingsBloc, SettingsBlocState>(
                  builder: (context, state) {
                    if (_selectedSection == SettingsSection.General) return GeneralRegion(state: state, fbKey: _fbKey);
                    return Container();
                  },
                )),
            builder: (BuildContext context) => blocFactory.getSettingsBloc(),
          ),
        ],
      ),
    );
  }
}
