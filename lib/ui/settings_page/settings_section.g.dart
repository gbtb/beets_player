// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'settings_section.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const SettingsSection _$unknown = const SettingsSection._('Unknown');
const SettingsSection _$general = const SettingsSection._('General');
const SettingsSection _$keyBindings = const SettingsSection._('KeyBindings');

SettingsSection _$valueOf(String name) {
  switch (name) {
    case 'Unknown':
      return _$unknown;
    case 'General':
      return _$general;
    case 'KeyBindings':
      return _$keyBindings;
    default:
      return _$unknown;
  }
}

final BuiltSet<SettingsSection> _$values =
    new BuiltSet<SettingsSection>(const <SettingsSection>[
  _$unknown,
  _$general,
  _$keyBindings,
]);

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
