import 'dart:async';

import 'package:beets_player/adapters/serializers.dart';
import 'package:beets_player/blocs/settings/settings_bloc_state.dart';
import 'package:beets_player/core/models/settings/general_settings.dart';
import 'package:beets_player/core/models/settings/settings_save_dir.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class GeneralRegion extends StatelessWidget {
  final SettingsBlocState state;
  final GlobalKey<FormBuilderState> fbKey;

  /// separated Stream for changing state of buttons, in order to left this Widget stateless. Otherwise multiple rebuild will occur, and reset wont work
  final StreamController<bool> changeController = StreamController<bool>();

  /// separated Stream for settings save path change, reason - the same as above
  final StreamController<bool> settingsDirChangeController = StreamController<bool>();

  GeneralRegion({
    Key key,
    this.state,
    this.fbKey,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          FormBuilder(
            key: fbKey,
            onChanged: (values) {
              changeController.add(true);
              final settingsDir = values["settingsSaveDir"];
              if (settingsDir != null) {
                final settingsDirEnum = serializers.deserializeWith(SettingsSaveDir.serializer, settingsDir);
                settingsDirChangeController.add(settingsDirEnum != SettingsSaveDir.SaveInCustomLocation);
              }
            },
            initialValue: serializers.serializeWith(GeneralSettings.serializer, state.generalSettings),
            child: Column(children: <Widget>[
              FormBuilderTextField(
                attribute: 'beetsDatabaseUri',
                decoration: InputDecoration(labelText: "Path to beets db"),
                validators: [],
              ),
              FormBuilderTextField(
                attribute: 'beetsLibraryDir',
                decoration: InputDecoration(labelText: "The directory of beets library"),
                validators: [],
              ),
              FormBuilderDropdown(
                attribute: "settingsSaveDir",
                decoration: InputDecoration(labelText: "Directory for saving beets_player settings"),
                initialValue:
                    serializers.serializeWith(SettingsSaveDir.serializer, state.generalSettings.settingsSaveDir),
                hint: Text('Select directory'),
                validators: [FormBuilderValidators.required()],
                items: SettingsSaveDir.values
                    .map((item) => DropdownMenuItem(
                        value: serializers.serializeWith(SettingsSaveDir.serializer, item),
                        child: Text(item.toHumanString())))
                    .toList(),
              ),
              StreamBuilder<bool>(
                  stream: settingsDirChangeController.stream,
                  initialData: true,
                  builder: (context, snapshot) {
                    return FormBuilderTextField(
                      attribute: 'settingsSaveUri',
                      decoration: InputDecoration(labelText: "Directory"),
                      validators: [],
                      readOnly: snapshot.data,
                    );
                  }),
              EditSettingsButtons(fbKey, changeController.stream)
            ]),
          )
        ],
      ),
    );
  }
}

class EditSettingsButtons extends StatelessWidget {
  final GlobalKey<FormBuilderState> _fbKey;
  final Stream<bool> _changeStream;

  EditSettingsButtons(this._fbKey, this._changeStream, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
        stream: _changeStream,
        initialData: false,
        builder: (context, snapshot) {
          return Row(
            children: <Widget>[
              if (snapshot.data)
                MaterialButton(
                  child: Text("Save"),
                  onPressed: () {
                    _fbKey.currentState.save();
                  },
                ),
              if (snapshot.data)
                MaterialButton(
                  child: Text("Reset"),
                  onPressed: () {
                    _fbKey.currentState.reset();
                  },
                ),
            ],
          );
        });
  }
}
