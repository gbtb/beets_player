import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
part 'settings_section.g.dart';

class SettingsSection extends EnumClass {
  @BuiltValueEnumConst(fallback: true)
  static const SettingsSection Unknown = _$unknown;

  static const SettingsSection General = _$general;
  static const SettingsSection KeyBindings = _$keyBindings;

  const SettingsSection._(String name) : super(name);
  static BuiltSet<SettingsSection> get values => _$values;
  static SettingsSection valueOf(String name) => _$valueOf(name);
}
