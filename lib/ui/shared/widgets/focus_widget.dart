import 'package:beets_player/core/flutter_sdk/key_handler.dart';
import 'package:flutter/material.dart';

/// Properly made focus widget, which allows you to use focus node and dont shoot yourself in the leg
class ProperFocus extends StatefulWidget {
  /// Callback which handles key's events
  final bool Function(FocusNode, RawKeyEvent) onKey;

  /// Builder which should use focus node to do good stuff
  final Widget Function(BuildContext context, FocusNode focusNode) builder;

  ///Opional key handler to use instead of onKey
  final AbstractKeyHandler keyHandler;

  final bool attach;

  ProperFocus({Key key, @required this.builder, this.onKey, this.keyHandler, this.attach = false}) : super(key: key);

  _ProperFocusState createState() => _ProperFocusState();
}

class _ProperFocusState extends State<ProperFocus> {
  FocusNode _focusNode;

  @override
  void initState() {
    super.initState();
    _focusNode = FocusNode(onKey: this.widget.keyHandler != null ? this.widget.keyHandler.callback : this.widget.onKey);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (this.widget.attach) FocusScope.of(context).requestFocus(_focusNode);
  }

  @override
  void dispose() {
    super.dispose();
    _focusNode.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return this.widget.builder(context, _focusNode);
  }
}
