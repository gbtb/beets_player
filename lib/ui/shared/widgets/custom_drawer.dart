import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../routes.dart';

class CustomDrawer extends StatefulWidget {
  static final GlobalKey thisKey = GlobalKey(debugLabel: "nav drawer");
  const CustomDrawer({
    Key key,
  }) : super(key: key);

  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  NavigationRoute _currentPage = NavigationRoute.Player;
  NavigationRoute get currentPage => _currentPage;
  set currentPage(route) {
    setState(() {
      _currentPage = route;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          new NavigationTile(
              parentState: this, tileRoute: NavigationRoute.Player, icon: Icon(MdiIcons.radio), title: "Player"),
          new NavigationTile(
              parentState: this, tileRoute: NavigationRoute.Settings, icon: Icon(MdiIcons.wrench), title: "Settings"),
        ],
      ),
    );
  }
}

class NavigationTile extends StatelessWidget {
  const NavigationTile({
    Key key,
    this.parentState,
    this.tileRoute,
    this.icon,
    this.title,
  }) : super(key: key);
  final _CustomDrawerState parentState;
  final NavigationRoute tileRoute;
  final Icon icon;
  final String title;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: icon,
      title: Text(title),
      onTap: () {
        Navigator.pushNamed(context, tileRoute.toString());
        parentState.currentPage = tileRoute;
      },
      selected: parentState.currentPage == tileRoute,
    );
  }
}
