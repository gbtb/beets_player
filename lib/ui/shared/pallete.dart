import 'dart:ui';

import 'package:flutter/material.dart';

class Pallete {
  static const Color PrimaryColor = Colors.white;
  static const Color BackgroundColor = Colors.white;
  static const Color PrimaryInverted = Colors.black87;
  static const Color AccentColor = Color.fromARGB(255, 128, 117, 255);
}
