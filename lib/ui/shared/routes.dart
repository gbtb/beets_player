import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
part 'routes.g.dart';

class NavigationRoute extends EnumClass {
  @BuiltValueEnumConst(fallback: true)
  static const NavigationRoute Unknown = _$unknown;

  static const NavigationRoute Player = _$player;
  static const NavigationRoute Settings = _$settings;
  const NavigationRoute._(String name) : super(name);
  static BuiltSet<NavigationRoute> get values => _$values;
  static NavigationRoute valueOf(String name) => _$valueOf(name);
}
