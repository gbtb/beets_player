import 'package:beets_player/blocs/bloc_factory.dart';
import 'package:beets_player/blocs/keyboard/keyboard_bloc.dart';
import 'package:beets_player/blocs/player/player_bloc.dart';
import 'package:beets_player/blocs/playlists/playlists_bloc.dart';
import 'package:beets_player/blocs/search/search_bloc.dart';
import 'package:beets_player/ui/main_page/player_region.dart';
import 'package:beets_player/ui/main_page/search_region.dart';
import 'package:beets_player/ui/main_page/widgets/status_bar.dart';
import 'package:beets_player/ui/shared/widgets/custom_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'content_region.dart';
import 'widgets/error_overlay.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  void initState() {
    super.initState();
    final errBloc = blocFactory.getErrorBloc();
    errBloc.state.skip(1).listen((state) {
      ErrorOverlay().show(context, "${state.lastError.source}: ${state.lastError.error.message}");
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final keyboardBloc = blocFactory.getKeyboardBloc();
    final playerBloc = blocFactory.getPlayerBloc();

    return MultiBlocProvider(
      providers: [
        BlocProvider<SearchBloc>(
          builder: (_) => blocFactory.getSearchBloc(),
        ),
        BlocProvider<PlaylistsBloc>(
          builder: (_) => blocFactory.getPlaylistsBloc(),
        ),
        BlocProvider<PlayerBloc>(
          builder: (_) => playerBloc,
        ),
        BlocProvider<KeyboardBloc>(
          builder: (_) => keyboardBloc,
        ),
      ],
      child: Shortcuts(
        shortcuts: Map.fromEntries([MapEntry(LogicalKeySet(LogicalKeyboardKey.space), Intent(Key("play")))]),
        child: Actions(
          child: DefaultFocusTraversal(
            policy: WidgetOrderFocusTraversalPolicy(),
            child: FocusScope(
              autofocus: true,
              child: Scaffold(
                  appBar: AppBar(
                    title: Text("Beets Player"),
                  ),
                  drawer: CustomDrawer(key: CustomDrawer.thisKey),
                  body: Column(
                    children: <Widget>[PlayerRegion(), SearchRegion(), ContentRegion(), StatusBar()],
                  )),
              onKey: (focusNode, event) {
                if (event is RawKeyDownEvent) return false;
                if (event.data.logicalKey == LogicalKeyboardKey.tab) {
                  if (!event.data.isShiftPressed) {
                    return focusNode.nextFocus();
                  } else {
                    return focusNode.previousFocus();
                  }
                }

                return true;
              },
            ),
          ),
          actions: Map.fromIterable(<CallbackAction>[
            CallbackAction(Key("play"), onInvoke: (_, __) {
              print("callback");
              playerBloc.dispatch(TogglePlay());
            })
          ], key: (a) => a.intentKey, value: (v) => () => v),
        ),
      ),
    );
  }
}
