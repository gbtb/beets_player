import 'package:beets_player/blocs/bloc_factory.dart';
import 'package:beets_player/blocs/keyboard/keyboard_bloc.dart';
import 'package:beets_player/blocs/keyboard/keyboard_bloc.dart';
import 'package:beets_player/blocs/keyboard/keyboard_bloc_state.dart';
import 'package:beets_player/blocs/player/player_bloc.dart';
import 'package:beets_player/blocs/player/player_bloc_state.dart';
import 'package:beets_player/blocs/playlists/playlists_bloc.dart';
import 'package:beets_player/blocs/playlists/playlists_bloc_state.dart';
import 'package:beets_player/blocs/search/search_bloc.dart';
import 'package:beets_player/core/flutter_sdk/key_handler.dart';
import 'package:beets_player/ui/main_page/widgets/playlists_tab_widget.dart';
import 'package:beets_player/ui/shared/pallete.dart';
import 'package:beets_player/ui/shared/widgets/focus_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchRegion extends StatefulWidget {
  const SearchRegion({Key key}) : super(key: key);

  @override
  _SearchRegionState createState() => _SearchRegionState();
}

class _SearchRegionState extends State<SearchRegion> {
  SearchBloc _searchBloc;

  PlaylistsBloc _playlistsBloc;

  TextEditingController _controller;

  PlayerBloc _playerBloc;

  @override
  void initState() {
    super.initState();
    _searchBloc = BlocProvider.of<SearchBloc>(context);
    _playlistsBloc = BlocProvider.of<PlaylistsBloc>(context);
    _playerBloc = BlocProvider.of<PlayerBloc>(context);
    _controller = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final enterKeyHandler =
        VoidKeyHandler(LogicalKeyboardKey.enter, (_) => _searchBloc.dispatch(PerformSimpleSearch(_controller.text)));

    return Container(
      child: BlocBuilder<PlayerBloc, PlayerBlocState>(
          bloc: _playerBloc,
          builder: (context, player) {
            return BlocBuilder<PlaylistsBloc, PlaylistsBlocState>(
                bloc: _playlistsBloc,
                builder: (context, playlists) {
                  return Row(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    textBaseline: TextBaseline.alphabetic,
                    children: <Widget>[
                      Container(
                        child: ProperFocus(
                          builder: (BuildContext context, FocusNode focusNode) {
                            return TextField(
                                controller: _controller,
                                focusNode: focusNode,
                                decoration:
                                    InputDecoration(border: OutlineInputBorder(), focusedBorder: OutlineInputBorder()));
                          },
                          keyHandler: enterKeyHandler,
                        ),
                        width: 200,
                      ),
                      Container(
                        width: 20,
                      ),
                      Container(
                          child: ProperFocus(
                            builder: (BuildContext context, FocusNode focusNode) {
                              return FlatButton(
                                child: Text("Search"),
                                onPressed: () => _searchBloc.dispatch(PerformSimpleSearch(_controller.text)),
                                focusNode: focusNode,
                              );
                            },
                            keyHandler: enterKeyHandler,
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(3.0),
                              border: Border.all(width: 1, color: Pallete.AccentColor))),
                      ...playlists.playlists.map((p) => PlaylistTabWidget(
                          p,
                          playlists.currentPlaylist,
                          player,
                          () => _playlistsBloc.dispatch(ClosePlaylist(p)),
                          () => {_playlistsBloc.dispatch(SetSelectedPlaylist(p))})),
                    ],
                  );
                });
          }),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
      height: 40,
      decoration: BoxDecoration(
          boxShadow: [BoxShadow(spreadRadius: 4.0, blurRadius: 2.0, color: Colors.black26)],
          color: Pallete.BackgroundColor),
    );
  }
}
