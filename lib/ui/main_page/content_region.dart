import 'package:beets_player/blocs/player/player_bloc.dart';
import 'package:beets_player/blocs/player/player_bloc_state.dart';
import 'package:beets_player/blocs/playlist/playlist_bloc.dart';
import 'package:beets_player/blocs/playlist/playlist_bloc_state.dart';
import 'package:beets_player/blocs/playlist_scroll/playlist_scroll_bloc.dart' as scroll;
import 'package:beets_player/blocs/playlists/playlists_bloc.dart';
import 'package:beets_player/blocs/playlists/playlists_bloc_state.dart';
import 'package:beets_player/blocs/search/search_bloc.dart';
import 'package:beets_player/blocs/search/search_bloc_state.dart';
import 'package:beets_player/core/flutter_sdk/key_handler.dart';
import 'package:beets_player/core/models/library_item.dart';
import 'package:beets_player/core/sdk/time/duration.dart';
import 'package:beets_player/ui/main_page/widgets/button_with_tooltip.dart';
import 'package:beets_player/ui/shared/pallete.dart';
import 'package:beets_player/ui/shared/widgets/focus_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class ContentRegion extends StatefulWidget {
  const ContentRegion({Key key}) : super(key: key);

  @override
  _ContentRegionState createState() => _ContentRegionState();
}

class _ContentRegionState extends State<ContentRegion> {
  @override
  Widget build(BuildContext context) {
    final _searchBloc = BlocProvider.of<SearchBloc>(context);
    final _playlistsBloc = BlocProvider.of<PlaylistsBloc>(context);
    final _playerBloc = BlocProvider.of<PlayerBloc>(context);

    return BlocBuilder<SearchBloc, SearchBlocState>(
        bloc: _searchBloc,
        builder: (ctx, state) {
          return Expanded(
            child: Row(
              children: <Widget>[
                if (state.resultList.isNotEmpty) SidebarButtons(playlistsBloc: _playlistsBloc, searchBloc: _searchBloc),
                Expanded(
                    child: Scrollbar(
                  child: state.resultList.isEmpty
                      ? Text("No search results")
                      : ListView.builder(
                          itemBuilder: (context, index) => LibraryListItem(
                                decoration: null,
                                index: index,
                                selected: false,
                                songItem: state.resultList[index],
                              ),
                          itemCount: state.resultList.length),
                )),
                PlaylistRegion(playerBloc: _playerBloc, playlistsBloc: _playlistsBloc)
              ],
            ),
          );
        });
  }
}

class SidebarButtons extends StatelessWidget {
  const SidebarButtons({
    Key key,
    @required PlaylistsBloc playlistsBloc,
    @required SearchBloc searchBloc,
  })  : _playlistsBloc = playlistsBloc,
        _searchBloc = searchBloc,
        super(key: key);

  final PlaylistsBloc _playlistsBloc;
  final SearchBloc _searchBloc;

  @override
  Widget build(BuildContext context) {
    final state = _searchBloc.currentState;

    return Column(
      children: <Widget>[
        new ButtonWithTooltip(
          child: Icon(MdiIcons.clipboardPlay),
          tooltip: "Play in a new Playlist",
          onPressed: () => _playlistsBloc.dispatch(CreateNewPlaylistAndStartPlaying(state.query, state.resultList)),
        ),
        new ButtonWithTooltip(
          child: Icon(MdiIcons.clipboardPlus),
          tooltip: "Create new Playlist",
          onPressed: () => _playlistsBloc.dispatch(CreateNewPlaylist(state.query, state.resultList)),
        ),
        new ButtonWithTooltip(
          child: Icon(MdiIcons.clipboardText),
          tooltip: "Append to current Playlist",
          onPressed: () => _playlistsBloc.dispatch(AppendToCurrentPlaylist(_searchBloc.currentState.resultList)),
        ),
      ],
    );
  }
}

Widget Function(BuildContext, int) _displayItem(PlaylistBloc playlistBloc, PlaylistBlocState state,
        LibraryItem currentlyPlayingItem, VoidCallback onSelect(LibraryItem item)) =>
    (BuildContext context, int index) {
      final key = index == 1 ? GlobalKey() : null;
      if (index == 1) {
        playlistBloc.scrollBloc.dispatch(scroll.KeyToGetItemSize(key));
      }
      final songItem = state.items[index];
      final currentlyPlaying = songItem == currentlyPlayingItem;
      final selected = index == state.selectedIndex;
      final decoration = currentlyPlaying || selected
          ? BoxDecoration(
              color: selected ? Pallete.PrimaryInverted : null,
              border: currentlyPlaying ? Border.all(color: Pallete.AccentColor, width: 1) : null,
              borderRadius: BorderRadius.all(Radius.circular(3)))
          : null;

      return LibraryListItem(
        key: key,
        decoration: decoration,
        songItem: songItem,
        selected: selected,
        index: index,
      );
    };

class LibraryListItem extends StatelessWidget {
  final VoidCallback Function(LibraryItem item) onSelect;

  final int index;

  const LibraryListItem(
      {Key key,
      @required this.decoration,
      @required this.songItem,
      @required this.selected,
      @required this.index,
      this.onSelect})
      : super(key: key);

  final BoxDecoration decoration;
  final LibraryItem songItem;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      decoration: decoration,
      child: ListTile(
        onTap: onSelect != null ? onSelect(songItem) : null,
        title: Text(
          "${songItem.title} - ${songItem.album}",
        ),
        subtitle: Text(
          songItem.artist,
        ),
        trailing: Text(
          DurationFormatter.format(songItem.duration),
          style: TextStyle(color: selected ? Pallete.PrimaryColor : Pallete.PrimaryInverted),
        ),
        selected: selected,
      ),
    );
  }
}

class PlaylistRegion extends StatefulWidget {
  const PlaylistRegion({
    Key key,
    @required PlayerBloc playerBloc,
    @required PlaylistsBloc playlistsBloc,
  })  : _playerBloc = playerBloc,
        _playlistsBloc = playlistsBloc,
        super(key: key);

  final PlayerBloc _playerBloc;
  final PlaylistsBloc _playlistsBloc;

  @override
  _PlaylistRegionState createState() => _PlaylistRegionState();
}

class _PlaylistRegionState extends State<PlaylistRegion> {
  ScrollController controller;
  int scrolledToIndex;
  PlaylistBloc currentPlaylist;

  @override
  void initState() {
    super.initState();
    scrolledToIndex = 0;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PlaylistsBloc, PlaylistsBlocState>(
        bloc: widget._playlistsBloc,
        builder: (context, playlists) {
          return Expanded(
              child: Scrollbar(
                  child: playlists.currentPlaylist == null
                      ? Text("No playlist selected", textAlign: TextAlign.center)
                      : Column(children: [
                          BlocBuilder<PlaylistBloc, PlaylistBlocState>(
                              bloc: playlists.currentPlaylist,
                              builder: (context, snapshot) {
                                return BlocBuilder<PlayerBloc, PlayerBlocState>(
                                    bloc: widget._playerBloc,
                                    builder: (context, player) {
                                      final playlistKeyHandler = KeyHandlerComposition([
                                        VoidKeyHandler(
                                            LogicalKeyboardKey.arrowDown,
                                            (_) => widget._playlistsBloc.currentState.currentPlaylist
                                                .dispatch(MoveSelectionDown())),
                                        VoidKeyHandler(
                                            LogicalKeyboardKey.arrowUp,
                                            (_) => widget._playlistsBloc.currentState.currentPlaylist
                                                .dispatch(MoveSelectionUp())),
                                        VoidKeyHandler(
                                            LogicalKeyboardKey.enter,
                                            (_) => widget._playerBloc.dispatch(SetPlaylistAndItem(
                                                playlists.currentPlaylist,
                                                widget._playlistsBloc.currentState.currentPlaylist.currentState
                                                    .selectedItem))),
                                      ]);
                                      return ProperFocus(
                                        keyHandler: playlistKeyHandler,
                                        attach: true,
                                        builder: (BuildContext context, FocusNode focusNode) {
                                          final currentPlaylist = widget._playlistsBloc.currentState.currentPlaylist;

                                          return Expanded(
                                            child: ListView.builder(
                                                key: ValueKey(currentPlaylist.currentState.name),
                                                controller: currentPlaylist.scrollBloc.currentState.controller,
                                                itemBuilder: _displayItem(
                                                  currentPlaylist,
                                                  playlists.currentPlaylist.currentState,
                                                  player.currentItem,
                                                  (item) =>
                                                      () => playlists.currentPlaylist.dispatch(SetSelectedItem(item)),
                                                ),
                                                itemCount: playlists.currentPlaylist.currentState.items.length),
                                          );
                                        },
                                      );
                                    });
                              }),
                          BlocListener<PlaylistsBloc, PlaylistsBlocState>(
                            bloc: widget._playlistsBloc,
                            condition: (prevState, newState) => prevState.currentPlaylist != newState.currentPlaylist,
                            listener: (BuildContext context, PlaylistsBlocState playlists) {
                              setState(() {
                                currentPlaylist = playlists.currentPlaylist;
                                scrolledToIndex = currentPlaylist?.currentState?.selectedIndex;
                              });
                            },
                            child: new Container(
                              width: 0,
                              height: 0,
                            ),
                          ),
                          if (widget._playlistsBloc?.currentState?.currentPlaylist != null)
                            BlocListener<PlaylistBloc, PlaylistBlocState>(
                              bloc: playlists.currentPlaylist,
                              condition: (prevState, newState) =>
                                  prevState.selectedIndex != newState.selectedIndex && newState.selectedIndex != null,
                              listener: (context, playlist) {
                                setState(() {
                                  scrolledToIndex = playlist.selectedIndex;
                                });
                              },
                              child: new Container(
                                width: 0,
                                height: 0,
                              ),
                            )
                        ])));
        });
  }
}
