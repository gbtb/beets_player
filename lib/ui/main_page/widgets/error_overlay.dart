import 'package:flutter/material.dart';

/// Responsible for displaying overlay with error text
/// used tutor from https://www.didierboelens.com/2018/06/how-to-create-a-toast-or-notifications-notion-of-overlay/
class ErrorOverlay {
  String _msgText;

  void show(BuildContext context, String msgText) async {
    _msgText = msgText;
    OverlayState overlayState = Overlay.of(context);
    OverlayEntry overlayEntry = OverlayEntry(builder: _build);

    overlayState.insert(overlayEntry);

    await Future.delayed(const Duration(seconds: 5));

    overlayEntry.remove();
  }

  Widget _build(BuildContext context) {
    return Positioned(
      bottom: 20.0,
      left: 20.0,
      right: 20.0,
      child: Material(
          color: Colors.white,
          elevation: 4,
          child: Container(
            height: 100,
            child: Row(
              children: <Widget>[Icon(Icons.warning, color: Colors.yellow), Flexible(child: Text(_msgText))],
            ),
          )),
    );
  }
}
