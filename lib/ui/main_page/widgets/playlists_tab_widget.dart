import 'package:beets_player/blocs/player/playback_state.dart';
import 'package:beets_player/blocs/player/player_bloc_state.dart';
import 'package:beets_player/blocs/playlist/playlist_bloc.dart';
import 'package:beets_player/ui/shared/pallete.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class PlaylistTabWidget extends StatelessWidget {
  final void Function() _onTabClosePressed;

  final void Function() _onTabClicked;

  final PlaylistBloc _playlist;

  final PlaylistBloc _selectedPlaylist;

  final PlayerBlocState _playerState;

  const PlaylistTabWidget(
      this._playlist, this._selectedPlaylist, this._playerState, this._onTabClosePressed, this._onTabClicked,
      {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var decoration = _playlist == _selectedPlaylist
        ? BoxDecoration(
            gradient: LinearGradient(
                colors: [Colors.black38, Pallete.PrimaryColor, Colors.black38],
                stops: [0.05, 0.5, 0.95],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter))
        : BoxDecoration(
            border: Border(
            right: BorderSide(width: 1, color: Pallete.PrimaryInverted),
          ));

    final name = _playlist.currentState.name;
    final text = name.length > 13 ? name.substring(0, name.length - 3) + "..." : name;

    return GestureDetector(
      child: Container(
        child: Row(
          children: <Widget>[
            if (_playerState.playlistBloc == _playlist && _playerState.playbackState == PlaybackState.Playing)
              Icon(MdiIcons.play),
            if (_playerState.playlistBloc == _playlist && _playerState.playbackState == PlaybackState.Paused)
              Icon(MdiIcons.pause),
            Text(text),
            IconButton(
              icon: Icon(MdiIcons.close),
              onPressed: _onTabClosePressed,
            )
          ],
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
        ),
        decoration: decoration,
        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 5),
      ),
      onTap: _onTabClicked,
    );
  }
}
