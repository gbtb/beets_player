import 'package:beets_player/blocs/playlist/playlist_bloc.dart';
import 'package:beets_player/blocs/playlist/playlist_bloc_state.dart';
import 'package:beets_player/blocs/playlists/playlists_bloc.dart';
import 'package:beets_player/blocs/playlists/playlists_bloc_state.dart';
import 'package:beets_player/blocs/search/search_bloc.dart';
import 'package:beets_player/blocs/search/search_bloc_state.dart';
import 'package:beets_player/core/sdk/time/duration.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class StatusBar extends StatelessWidget {
  const StatusBar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocBuilder<SearchBloc, SearchBlocState>(
        builder: (BuildContext context, SearchBlocState searchState) {
          return BlocBuilder<PlaylistsBloc, PlaylistsBlocState>(
            builder: (BuildContext context, PlaylistsBlocState playlistsState) {
              return Material(
                elevation: 100,
                shadowColor: Colors.black,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("${searchState.resultList.length} items found"),
                    if (playlistsState.currentPlaylist != null)
                      BlocBuilder<PlaylistBloc, PlaylistBlocState>(
                        bloc: playlistsState.currentPlaylist,
                        builder: (BuildContext context, PlaylistBlocState playlist) => Text(
                            "${playlist.items.length} items, total duration ${DurationFormatter.format(playlist.totalDuration)}"),
                      )
                  ],
                ),
              );
            },
          );
        },
      ),
    );
  }
}
