import 'package:beets_player/core/flutter_sdk/key_handler.dart';
import 'package:beets_player/ui/shared/widgets/focus_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ButtonWithTooltip extends StatefulWidget {
  final String tooltip;
  final Widget child;
  final void Function() onPressed;

  const ButtonWithTooltip({this.tooltip, this.child, this.onPressed, Key key}) : super(key: key);

  @override
  _ButtonWithTooltipState createState() => _ButtonWithTooltipState();
}

class _ButtonWithTooltipState extends State<ButtonWithTooltip> {
  @override
  Widget build(BuildContext context) {
    return ProperFocus(
      keyHandler: VoidKeyHandler(LogicalKeyboardKey.enter, (_) => this.widget.onPressed()),
      builder: (context, focusNode) => Tooltip(
          child: FlatButton(
            child: this.widget.child,
            onPressed: this.widget.onPressed,
            focusNode: focusNode,
          ),
          message: this.widget.tooltip),
    );
  }
}
