import 'dart:math';

import 'package:beets_player/blocs/player/playback_state.dart';
import 'package:beets_player/blocs/player/player_bloc.dart';
import 'package:beets_player/blocs/player/player_bloc_state.dart';
import 'package:beets_player/core/flutter_sdk/key_handler.dart';
import 'package:beets_player/core/models/volume_level.dart';
import 'package:beets_player/core/sdk/time/duration.dart';
import 'package:beets_player/ui/shared/pallete.dart';
import 'package:beets_player/ui/shared/widgets/focus_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class PlayerRegion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final PlayerBloc _playerBloc = BlocProvider.of<PlayerBloc>(context);

    return BlocBuilder<PlayerBloc, PlayerBlocState>(
      builder: (context, snapshot) {
        final playButtonPressed = (PlayerBlocState state) =>
            state.playbackState == PlaybackState.Playing ? _playerBloc.dispatch(Pause()) : _playerBloc.dispatch(Play());
        return Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              ProperFocus(
                keyHandler:
                    VoidKeyHandler(LogicalKeyboardKey.enter, (_) => playButtonPressed(_playerBloc.currentState)),
                builder: (BuildContext context, FocusNode focusNode) {
                  return RawMaterialButton(
                    focusNode: focusNode,
                    child: snapshot.playbackState == PlaybackState.Playing
                        ? Icon(MdiIcons.pauseCircle)
                        : Icon(MdiIcons.playCircle),
                    onPressed: () => playButtonPressed(snapshot),
                  );
                },
              ),
              ProperFocus(
                  keyHandler: VoidKeyHandler(LogicalKeyboardKey.enter, (_) => _playerBloc.dispatch(Stop())),
                  builder: (BuildContext context, FocusNode focusNode) => RawMaterialButton(
                        shape: CircleBorder(side: BorderSide(width: 1.0)),
                        child: Icon(MdiIcons.stopCircle),
                        onPressed: () => _playerBloc.dispatch(Stop()),
                        focusNode: focusNode,
                      )),
              Column(
                children: <Widget>[
                  volumeSlider(snapshot, _playerBloc),
                  Row(
                    children: <Widget>[
                      Text("${snapshot.volume.volume.round()}"),
                      IconButton(
                        icon: snapshot.isMuted ? Icon(MdiIcons.volumeMute) : Icon(MdiIcons.volumeMedium),
                        onPressed: () => _playerBloc.dispatch(SetMuted(!snapshot.isMuted)),
                      )
                    ],
                  )
                ],
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          if (snapshot.currentItem != null)
                            Container(child: Text("${snapshot.currentItem.artist} - ${snapshot.currentItem.title}")),
                          if (snapshot.currentItem != null)
                            Container(
                                child: Text(
                                    "${DurationFormatter.format(snapshot.currentItem.duration - snapshot.remainedDuration)} / ${DurationFormatter.format(snapshot.currentItem.duration)}")),
                        ],
                      ),
                    ),
                    SliderTheme(
                      child: Slider(
                        value: getSongSliderValue(snapshot),
                        min: 0,
                        max: 1,
                        onChanged: (double value) {},
                      ),
                      data: _sliderThemeData,
                    )
                  ],
                ),
              )
            ],
          ),
          height: 100,
        );
      },
      bloc: _playerBloc,
    );
  }

  double getSongSliderValue(PlayerBlocState snapshot) => snapshot.currentItem != null
      ? (snapshot.currentItem.duration - snapshot.remainedDuration).inSeconds / snapshot.currentItem.duration.inSeconds
      : 0;

  Transform volumeSlider(PlayerBlocState snapshot, PlayerBloc _playerBloc) {
    return Transform.rotate(
      angle: -pi / 2,
      child: Container(
        width: 80,
        height: 40,
        child: SliderTheme(
            child: Slider(
              value: snapshot.volume.volume,
              min: 0,
              max: 100,
              onChanged: (val) {
                _playerBloc.dispatch(SetVolume(VolumeLevel((b) => b..volume = val)));
              },
            ),
            data: _sliderThemeData),
      ),
    );
  }

  final SliderThemeData _sliderThemeData = SliderThemeData.fromPrimaryColors(
      primaryColor: Pallete.PrimaryInverted,
      primaryColorDark: Pallete.PrimaryColor,
      primaryColorLight: Pallete.AccentColor,
      valueIndicatorTextStyle: TextStyle(color: Colors.red));
}
