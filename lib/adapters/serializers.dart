import 'package:beets_player/adapters/uri_serializer.dart';
import 'package:beets_player/core/models/beets_config.dart';
import 'package:beets_player/core/models/library_item.dart';
import 'package:beets_player/core/models/settings/general_settings.dart';
import 'package:beets_player/core/models/settings/settings_save_dir.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';

part 'serializers.g.dart';

@SerializersFor([LibraryItem, BeetsConfig, GeneralSettings])
final Serializers serializers = (_$serializers.toBuilder()
      ..add(UriSerializer())
      ..addPlugin(StandardJsonPlugin()))
    .build();

//final Serializers serializers = _$serializers;
