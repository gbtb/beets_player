import 'package:beets_player/core/models/settings/general_settings.dart';
import 'package:beets_player/core/sdk/app_error.dart';
import 'package:beets_player/core/sdk/result.dart';
import 'package:beets_player/core/settings_repository.dart';

class SettingsRepository implements ISettingsRepository {
  @override
  Future<Result<AppError, GeneralSettings>> getGeneralSettings() {
    return Future.value(Result.fromErr(SettingsNotFoundError()));
  }
}
