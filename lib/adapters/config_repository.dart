import 'package:beets_player/adapters/serializers.dart';
import 'package:beets_player/core/beets_config_repository.dart';
import 'package:beets_player/core/models/beets_config.dart';
import 'package:beets_player/core/sdk/app_error.dart';
import 'package:beets_player/core/sdk/result.dart';
import 'package:flutter/services.dart';

class DesktopConfigRepository implements IBeetsConfigRepository {
  static const _channel = const MethodChannel('beets_config_plugin', const JSONMethodCodec());

  @override
  Future<Result<AppError, BeetsConfig>> findConfig() async {
    try {
      var res = await _channel.invokeMethod<dynamic>("find_config");
      var config = serializers.deserializeWith(BeetsConfig.serializer, res as Map<String, dynamic>);

      return Result.fromOk(config);
    } on PlatformException catch (e) {
      return Result.fromErr(CaughtException(e));
    } on Error catch (e) {
      return Result.fromErr(CaughtError(e));
    }
  }

  @override
  Future<Result<AppError, BeetsConfig>> getConfig(Uri path) {
    // TODO: implement getConfig
    return null;
  }
}
