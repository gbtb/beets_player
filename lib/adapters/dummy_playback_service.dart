import 'package:beets_player/core/models/library_item.dart';
import 'package:beets_player/core/models/volume_level.dart';
import 'package:beets_player/core/playback_service.dart';
import 'package:beets_player/core/sdk/result.dart';

class DummyPlaybackService implements IPlaybackService {
  @override
  Future<Result<PlaybackError, void>> pause() {
    return Future.value(Result.fromOk(null));
  }

  @override
  Future<Result<PlaybackError, void>> play() {
    return Future.value(Result.fromOk(Null));
  }

  @override
  Future<Result<PlaybackError, void>> setItem(LibraryItem item) {
    return Future.value(Result.fromOk(Null));
  }

  @override
  Future<Result<PlaybackError, void>> stop() {
    return Future.value(Result.fromOk(Null));
  }

  @override
  Future<Result<PlaybackError, void>> setMuted(bool isMuted) {
    return Future.value(Result.fromOk(Null));
  }

  @override
  Future<Result<PlaybackError, void>> setVolume(VolumeLevel volume) {
    return Future.value(Result.fromOk(Null));
  }
}
