import 'package:beets_player/core/models/library_item.dart';
import 'package:beets_player/core/models/volume_level.dart';
import 'package:beets_player/core/playback_service.dart';
import 'package:beets_player/core/sdk/app_error.dart';
import 'package:beets_player/core/sdk/result.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

class PlaybackService implements IPlaybackService {
  static const _channel = const MethodChannel('player_plugin', const JSONMethodCodec());

  Future<Result<PlaybackError, void>> _call(AsyncCallback callback) async {
    try {
      await callback();

      return Result.fromOk(Result.fromOk(Null));
    } on PlatformException catch (e) {
      return Result.fromErr(CaughtException(e));
    } on Error catch (e) {
      return Result.fromErr(CaughtError(e));
    }
  }

  @override
  Future<Result<PlaybackError, void>> pause() {
    return _call(() => _channel.invokeMethod<dynamic>("pause"));
  }

  @override
  Future<Result<PlaybackError, void>> play() async {
    return _call(() => _channel.invokeMethod<dynamic>("play"));
  }

  @override
  Future<Result<PlaybackError, void>> setItem(LibraryItem item) {
    return _call(() => _channel.invokeMethod<dynamic>("set_item", item.location.toString()));
  }

  @override
  Future<Result<PlaybackError, void>> stop() {
    return _call(() => _channel.invokeMethod<dynamic>("stop"));
  }

  @override
  Future<Result<PlaybackError, void>> setMuted(bool isMuted) {
    return _call(() => _channel.invokeMethod<dynamic>("mute", isMuted));
  }

  @override
  Future<Result<PlaybackError, void>> setVolume(VolumeLevel volume) {
    return _call(() => _channel.invokeMethod<dynamic>("set_volume", volume.volume / 100));
  }
}
