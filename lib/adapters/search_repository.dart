import 'package:beets_player/adapters/serializers.dart';
import 'package:beets_player/core/models/library_item.dart';
import 'package:beets_player/core/sdk/app_error.dart';
import 'package:beets_player/core/sdk/result.dart';
import 'package:beets_player/core/search_repository.dart';
import 'package:built_collection/built_collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class BeetsSearchRepository implements ISearchRepository {
  static const _channel = const MethodChannel('search_plugin', const JSONMethodCodec());

  @visibleForTesting
  BeetsSearchRepository();

  Future<Result<AppError, BeetsSearchRepository>> init(Uri pathToDatabase) async {
    try {
      await _channel.invokeMethod<dynamic>("init", pathToDatabase.toString());

      return Result.fromOk(this);
    } on PlatformException catch (e) {
      return Result.fromErr(CaughtException(e));
    } on Error catch (e) {
      return Result.fromErr(CaughtError(e));
    }
  }

  @override
  Future<Result<AppError, BuiltList<LibraryItem>>> simpleSearch(String query) async {
    try {
      var res = await _channel.invokeMethod<List<dynamic>>("simple_search", query);

      var v = res.map((x) => serializers.deserializeWith(LibraryItem.serializer, x as Map<String, dynamic>));

      return Result.fromOk(BuiltList.from(v));
    } on PlatformException catch (e) {
      return Result.fromErr(CaughtException(e));
    } on Error catch (e) {
      return Result.fromErr(CaughtError(e));
    }
  }
}
