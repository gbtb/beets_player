import 'package:beets_player/blocs/error/error_bloc.dart';
import 'package:beets_player/blocs/player/playback_state.dart';
import 'package:beets_player/blocs/player/player_bloc.dart';
import 'package:beets_player/blocs/player/player_bloc_state.dart';
import 'package:beets_player/blocs/playlist/playlist_bloc.dart';
import 'package:beets_player/core/models/library_item.dart';
import 'package:beets_player/core/models/volume_level.dart';
import 'package:beets_player/core/playback_service.dart';
import 'package:beets_player/core/sdk/app_error.dart';
import 'package:beets_player/core/sdk/result.dart';
import 'package:beets_player/core/sdk/time/time_provider.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:matcher/matcher.dart';
import 'package:mockito/mockito.dart';

import 'shared.dart';
import 'test_bloc_delegate.dart';

void main() {
  BlocSupervisor.delegate = new TestBlocDelegate();
  final song = LibraryItem((b) => b
    ..album = "Nebulous"
    ..artist = "Taylor Davis"
    ..title = "Taylor Davis"
    ..duration = Duration(minutes: 3));

  final song2 = LibraryItem((b) => b
    ..album = "Hidden Falls"
    ..artist = "Taylor Davis"
    ..title = "Taylor Davis"
    ..duration = Duration(seconds: 3));

  final song3 = LibraryItem((b) => b
    ..album = "Morning Star"
    ..artist = "Taylor Davis"
    ..title = "Taylor Davis"
    ..duration = Duration(seconds: 3));

  final timeFactory = TimeProvider(0.3);

  test("When playback service setItem fails, error bloc gets notification", () async {
    final playbackService = TestPlaybackService();
    when(playbackService.setItem(any)).thenAnswer((_) => Future.value(Result.fromErr(ItemNotFoundError())));
    final errorBloc = new ErrorBloc();
    final playerBloc = PlayerBloc(playbackService, errorBloc, timeFactory);

    final playlistBloc = PlaylistBloc("", <LibraryItem>[song], playerBloc, null, null);

    playerBloc.dispatch(SetPlaylist(playlistBloc));

    await for (var state in errorBloc.state.skip(1)) {
      expect(state.lastError.error, TypeMatcher<ItemNotFoundError>());
      expect(state.lastError.source, "Player");
      break;
    }

    expect(playerBloc.currentState.playbackState, PlaybackState.Unknown);
  });

  test("When playback service pause fails, error bloc gets notification and playback state updates to unknown",
      () async {
    final playbackService = TestPlaybackService();
    when(playbackService.setItem(any)).thenAnswer((_) => Future.value(Result.fromOk(null)));
    when(playbackService.play()).thenAnswer((_) => Future.value(Result.fromOk(null)));
    when(playbackService.pause()).thenAnswer(
        (_) => Future.value(Result.fromErr(CaughtError(StateError("Some error during message pass to platform")))));
    final errorBloc = new ErrorBloc();
    final playerBloc = PlayerBloc(playbackService, errorBloc, timeFactory);

    final playlistBloc = PlaylistBloc("", <LibraryItem>[song], playerBloc, null, null);

    playerBloc.dispatch(SetPlaylist(playlistBloc));
    playerBloc.dispatch(Play());

    verifyPlaying(playerBloc, song, playbackService, 2);
    playerBloc.dispatch(Pause());

    await for (var state in errorBloc.state.skip(1)) {
      expect(state.lastError.error, TypeMatcher<CaughtError>());
      expect(state.lastError.source, "Player");
      expect(playerBloc.currentState.playbackState, PlaybackState.Unknown);
      break;
    }
  });

  test("When playback service stop fails, error bloc gets notification and playback state updates to unknown",
      () async {
    final playbackService = TestPlaybackService();
    when(playbackService.setItem(any)).thenAnswer((_) => Future.value(Result.fromOk(null)));
    when(playbackService.play()).thenAnswer((_) => Future.value(Result.fromOk(null)));
    when(playbackService.stop()).thenAnswer((_) => Future.value(Result.fromErr(ItemNotFoundError())));
    final errorBloc = new ErrorBloc();
    final playlistBloc = PlaylistBloc("", <LibraryItem>[song], null, null, null);
    var playerBloc = TestPlayerBloc(
        PlayerBlocState((b) => b
          ..currentItem.replace(song)
          ..playbackState = PlaybackState.Playing
          ..playlistBloc = playlistBloc
          ..volume.replace(VolumeLevel((b) => b..volume = 50))
          ..isMuted = false
          ..build()),
        playbackService,
        errorBloc,
        timeFactory);

    playerBloc.dispatch(Stop());

    await for (var state in errorBloc.state.skip(1)) {
      expect(state.lastError.error, TypeMatcher<ItemNotFoundError>());
      expect(state.lastError.source, "Player");
      expect(playerBloc.currentState.playbackState, PlaybackState.Unknown);
      break;
    }

    playerBloc = TestPlayerBloc(
        PlayerBlocState((b) => b
          ..currentItem.replace(song)
          ..playbackState = PlaybackState.Playing
          ..playlistBloc = playlistBloc
          ..volume.replace(VolumeLevel((b) => b..volume = 50))
          ..isMuted = false
          ..remainedDuration = Duration.zero
          ..build()),
        playbackService,
        errorBloc,
        timeFactory);

    playerBloc.dispatch(TimerTick());

    await for (var state in errorBloc.state.skip(1)) {
      expect(state.lastError.error, TypeMatcher<ItemNotFoundError>());
      expect(state.lastError.source, "Player");
      expect(playerBloc.currentState.playbackState, PlaybackState.Unknown);
      break;
    }
  });

  test("When playback service play fails, error bloc gets notification and playback state updates to unknown",
      () async {
    final playbackService = TestPlaybackService();
    when(playbackService.play()).thenAnswer((_) => Future.value(Result.fromErr(ItemNotFoundError())));
    final errorBloc = new ErrorBloc();
    final playlistBloc = PlaylistBloc("", <LibraryItem>[song], null, null, null);
    var playerBloc = TestPlayerBloc(
        PlayerBlocState((b) => b
          ..currentItem.replace(song)
          ..playbackState = PlaybackState.Stopped
          ..volume.replace(VolumeLevel((b) => b..volume = 50))
          ..playlistBloc = playlistBloc
          ..isMuted = false
          ..build()),
        playbackService,
        errorBloc,
        timeFactory);

    playerBloc.dispatch(Play());

    await for (var state in errorBloc.state.skip(1)) {
      expect(state.lastError.error, TypeMatcher<ItemNotFoundError>());
      expect(state.lastError.source, "Player");
      expect(playerBloc.currentState.playbackState, PlaybackState.Unknown);
      break;
    }
  });
}

class TestPlaybackService extends Mock implements IPlaybackService {}

class TestPlayerBloc extends PlayerBloc {
  PlayerBlocState _initialState;

  TestPlayerBloc(this._initialState, IPlaybackService playbackService, ErrorBloc errorBloc, ITimeProvider timeProvider)
      : super(playbackService, errorBloc, timeProvider);

  @override
  PlayerBlocState get initialState => _initialState;
  set initialState(PlayerBlocState st) {
    _initialState = st;
  }
}
