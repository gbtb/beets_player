import 'package:beets_player/blocs/bloc_factory.dart';
import 'package:beets_player/blocs/player/playback_state.dart';
import 'package:beets_player/blocs/player/player_bloc.dart';
import 'package:beets_player/blocs/playlist/playlist_bloc.dart';
import 'package:beets_player/blocs/playlists/playlists_bloc.dart';
import 'package:beets_player/blocs/search/search_bloc.dart';
import 'package:beets_player/core/beets_config_repository.dart';
import 'package:beets_player/core/models/beets_config.dart';
import 'package:beets_player/core/models/library_item.dart';
import 'package:beets_player/core/playback_service.dart';
import 'package:beets_player/core/sdk/app_error.dart';
import 'package:beets_player/core/sdk/result.dart';
import 'package:beets_player/core/sdk/time/time_provider.dart';
import 'package:beets_player/core/search_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:built_collection/src/list.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import 'test_bloc_delegate.dart';

void main() {
  BlocSupervisor.delegate = new TestBlocDelegate();
  final configRepo = TestConfigRepo();
  blocFactory = BlocFactory().init(configRepository: configRepo, searchRepository: TestSearchRepository());
  test("Search block finds items from repo", () {
    const searchStr = "taylor";
    var searchBloc = SearchBloc(null, null);

    searchBloc.dispatch(SetRepository(Result.fromOk(TestSearchRepository())));
    searchBloc.dispatch(PerformSimpleSearch(searchStr));
    expect(searchBloc.state.skip(1).map((s) => s.resultList.length), emits(1));
  });

  test("Can create playlist from search results", () async {
    //blocFactory = TestBlocFactory();
    const searchStr = "taylor";
    var searchBloc = SearchBloc(null, null);
    var playlistsBloc = PlaylistsBloc(null);

    expect(playlistsBloc.state.map((s) => s.playlists.length), emits(0));

    searchBloc.dispatch(SetRepository(Result.fromOk(TestSearchRepository())));
    searchBloc.dispatch(PerformSimpleSearch(searchStr));
    await for (var len in searchBloc.state.skip(1).map((s) => s.resultList.length)) {
      expect(len, equals(1));
      break;
    }

    var searchResult = searchBloc.currentState;

    playlistsBloc.dispatch(CreateNewPlaylist("", searchBloc.currentState.resultList));
    await for (var state in playlistsBloc.state.skip(1)) {
      expect(state.playlists.length, equals(1));
      var playlistState = playlistsBloc.currentState.playlists[0].currentState;
      expect(playlistState.items.length, equals(1));
      expect(playlistState.items[0], equals(searchResult.resultList[0]));

      break;
    }
  });

  test("Can create playlist from search results and start playing", () async {
    var songs = await TestSearchRepository().simpleSearch("");

    final playbackService = TestPlaybackService();
    when(playbackService.setItem(songs.ok[0])).thenAnswer((_) => Future.value(Result.fromOk(null)));
    when(playbackService.play()).thenAnswer((_) => Future.value(Result.fromOk(null)));

    final timeFactory = TimeProvider(0.3);
    final playerBloc = new PlayerBloc(playbackService, null, timeFactory);
    final playlistsBloc = PlaylistsBloc(playerBloc);

    playlistsBloc.dispatch(CreateNewPlaylistAndStartPlaying("", songs.ok));
    await for (var state in playlistsBloc.state.skip(1)) {
      expect(state.playlists.length, equals(1));
      var playlistState = playlistsBloc.currentState.playlists[0].currentState;
      expect(playlistState.items.length, equals(1));
      expect(playlistState.items[0], equals(songs.ok[0]));

      break;
    }

    await for (var state in playerBloc.state.skip(2)) {
      expect(state.currentItem, equals(songs.ok[0]));
      expect(state.playbackState, equals(PlaybackState.Playing));

      verify(playbackService.play());
      break;
    }
  });

  test("Can create playlist from search results and append to playlist", () async {
    var songs = await TestSearchRepository().simpleSearch("");

    final playbackService = TestPlaybackService();
    final timeFactory = TimeProvider(0.3);
    when(playbackService.setItem(songs.ok[0])).thenAnswer((_) => Future.value(Result.fromOk(null)));
    when(playbackService.play()).thenAnswer((_) => Future.value(Result.fromOk(null)));

    final playerBloc = new PlayerBloc(null, null, timeFactory);
    final playlistsBloc = PlaylistsBloc(playerBloc);

    var song1 = LibraryItem((b) => b
      ..artist = "Taylor Davis"
      ..album = "Game On: 2 player mode"
      ..title = "Gerudo Valley"
      ..duration = Duration());

    var song2 = LibraryItem((b) => b
      ..artist = "Taylor Davis"
      ..album = "Game On: 2 player mode"
      ..title = "Chocobo Medley"
      ..duration = Duration());

    playlistsBloc.dispatch(CreateNewPlaylist("", [song1]));

    PlaylistBloc pl;
    await for (var state in playlistsBloc.state.skip(1)) {
      expect(state.playlists.length, equals(1));
      expect(state.currentPlaylist, equals(playlistsBloc.currentState.playlists.first));

      pl = state.currentPlaylist;
      break;
    }

    playlistsBloc.dispatch(CreateNewPlaylist("", [song2]));
    await for (var _ in playlistsBloc.state.skip(1)) {
      break;
    }
    playlistsBloc.dispatch(SetSelectedPlaylist(playlistsBloc.currentState.playlists[1]));

    playlistsBloc.dispatch(AppendToCurrentPlaylist(songs.ok));

    await for (var state in playlistsBloc.state) {
      expect(state.playlists.length, equals(2));
      expect(state.currentPlaylist, isNot(equals(pl)));
      expect(state.currentPlaylist, equals(playlistsBloc.currentState.playlists[1]));

      await for (var playlistState in state.currentPlaylist.state.skip(1)) {
        expect(playlistState.items.length, equals(2));
        expect(playlistState.items[0], equals(song2));
        expect(playlistState.items[1], equals(songs.ok[0]));

        break;
      }

      break;
    }
  });
}

class TestPlaybackService extends Mock implements IPlaybackService {}

class TestConfigRepo extends Mock implements IBeetsConfigRepository {
  @override
  Future<Result<AppError, BeetsConfig>> findConfig() =>
      Future.value(Result.fromOk(BeetsConfig((b) => b..library = Uri.parse("http://example.com"))));
}

// class TestBlocFactory extends Mock implements BlocFactory {
//   PlaylistBloc getPlaylistBloc(String name, Iterable<LibraryItem> items) =>
//       PlaylistBloc(name, items, _playerBloc, _errorBloc, _timeProvider);
// }

class TestSearchRepository implements ISearchRepository {
  @override
  Future<Result<AppError, BuiltList<LibraryItem>>> simpleSearch(String query) {
    return Future.value(Result.fromOk(BuiltList.from(<LibraryItem>[
      LibraryItem((b) => b
        ..album = "Nebulous"
        ..artist = "Taylor Davis"
        ..title = "Awakening"
        ..duration = Duration())
    ])));
  }

  @override
  Future<Result<AppError, ISearchRepository>> init(Uri pathToDatabase) {
    return Future.value(Result.fromOk(this));
  }
}
