import 'package:beets_player/core/models/volume_level.dart';
import 'package:beets_player/core/sdk/lazy.dart';
import 'package:test_api/test_api.dart';

void main() {
  test("Lazy var created only once", () {
    final obj = new Object();
    var lazyObject = new Lazy<Object>(() => obj);

    expect(lazyObject.value, obj);
    expect(lazyObject.value, obj);
  });

  //todo: not working... bug report?
  // test("Volume level throws if voleume out of range", () {
  //   expect(VolumeLevel((b) => b..volume = 200), throwsA(TypeMatcher<ArgumentError>()));
  // });
}
