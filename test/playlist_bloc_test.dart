import 'package:beets_player/blocs/playlist/playlist_bloc.dart';
import 'package:beets_player/blocs/playlists/playlists_bloc.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test("Can delete playlist from playlists", () async {
    var bloc = PlaylistsBloc(null);
    bloc.dispatch(CreateNewPlaylist("first", []));
    bloc.dispatch(CreateNewPlaylist("second", []));
    bloc.dispatch(CreateNewPlaylist("thirt", []));

    PlaylistBloc playlistToDelete;
    await for (var state in bloc.state.skip(3)) {
      expect(state.playlists.length, 3);
      playlistToDelete = state.playlists[1];
      expect(state.playlists.contains(playlistToDelete), true);
      break;
    }

    bloc.dispatch(SetSelectedPlaylist(playlistToDelete));

    await for (var state in bloc.state.skip(1)) {
      expect(state.currentPlaylist, playlistToDelete);
      break;
    }

    bloc.dispatch(ClosePlaylist(playlistToDelete));

    await for (var state in bloc.state.skip(1)) {
      expect(state.playlists.length, 2);
      expect(state.playlists.contains(playlistToDelete), false);
      break;
    }
  });
}
