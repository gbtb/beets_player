import 'package:beets_player/blocs/player/player_bloc.dart';
import 'package:beets_player/core/models/library_item.dart';
import 'package:beets_player/core/playback_service.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

Future verifyPlaying(PlayerBloc playerBloc, LibraryItem song, IPlaybackService playbackService, int n) async {
  var prevDuration = song.duration.inSeconds;
  var firstTime = true;
  await for (var state in playerBloc.state.skip(5).take(n)) {
    if (firstTime) {
      verify(playbackService.play());
      firstTime = false;
    }
    expect(state.remainedDuration.inSeconds, lessThan(prevDuration));
    prevDuration = state.remainedDuration.inSeconds;
    print("y");
  }
}
