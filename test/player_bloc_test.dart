import 'package:beets_player/blocs/player/playback_state.dart';
import 'package:beets_player/blocs/player/player_bloc.dart';
import 'package:beets_player/blocs/player/player_bloc_state.dart';
import 'package:beets_player/blocs/playlist/playlist_bloc.dart';
import 'package:beets_player/core/models/library_item.dart';
import 'package:beets_player/core/playback_service.dart';
import 'package:beets_player/core/sdk/result.dart';
import 'package:beets_player/core/sdk/time/time_provider.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import 'shared.dart';
import 'test_bloc_delegate.dart';

void main() {
  BlocSupervisor.delegate = new TestBlocDelegate();
  final song = LibraryItem((b) => b
    ..album = "Nebulous"
    ..artist = "Taylor Davis"
    ..title = "Taylor Davis"
    ..duration = Duration(minutes: 3));

  final song2 = LibraryItem((b) => b
    ..album = "Hidden Falls"
    ..artist = "Taylor Davis"
    ..title = "Taylor Davis"
    ..duration = Duration(seconds: 3));

  final song3 = LibraryItem((b) => b
    ..album = "Morning Star"
    ..artist = "Taylor Davis"
    ..title = "Taylor Davis"
    ..duration = Duration(seconds: 3));

  final timeProvider = TimeProvider(0.3);
  test("PlayerBloc plays, pauses and counts duration", () async {
    final playbackService = TestPlaybackService();
    when(playbackService.setItem(any)).thenAnswer((_) => Future.value(Result.fromOk(null)));
    when(playbackService.play()).thenAnswer((_) => Future.value(Result.fromOk(null)));
    when(playbackService.pause()).thenAnswer((_) => Future.value(Result.fromOk(null)));
    final playerBloc = PlayerBloc(playbackService, null, timeProvider);

    final playlistBloc = PlaylistBloc("", <LibraryItem>[song], playerBloc, null, null);

    playerBloc.dispatch(SetPlaylist(playlistBloc));

    await for (var state in playerBloc.state.skip(1)) {
      expect(state.currentItem, playlistBloc.currentState.items.first);
      expect(state.playbackState, PlaybackState.Stopped);
      expect(state.remainedDuration, song.duration);

      break;
    }

    playerBloc.dispatch(Play());
    var prevDuration = song.duration.inSeconds;
    var firstTime = true;
    await for (var state in playerBloc.state.skip(2).take(4)) {
      if (firstTime) {
        verify(playbackService.play());
        firstTime = false;
      }
      expect(state.remainedDuration.inSeconds, lessThan(prevDuration));
      prevDuration = state.remainedDuration.inSeconds;
      print("x");
    }

    prevDuration = playerBloc.currentState.remainedDuration.inSeconds;
    playerBloc.dispatch(Pause());
    await Future.delayed(timeProvider.adjustDuration(Duration(seconds: 2)));
    verify(playbackService.pause());
    expect(playerBloc.currentState.remainedDuration.inSeconds, prevDuration);

    playerBloc.dispatch(Play());
    firstTime = true;
    await for (var state in playerBloc.state.skip(2).take(3)) {
      if (firstTime) {
        verify(playbackService.play());
        firstTime = false;
      }
      expect(state.remainedDuration.inSeconds, lessThan(prevDuration));
      prevDuration = state.remainedDuration.inSeconds;
      print("x");
    }
  });

  test("PlayerBloc plays playlist sequentially", () async {
    final playbackService = TestPlaybackService();
    when(playbackService.setItem(any)).thenAnswer((_) => Future.value(Result.fromOk(null)));
    when(playbackService.play()).thenAnswer((_) => Future.value(Result.fromOk(null)));
    when(playbackService.stop()).thenAnswer((_) => Future.value(Result.fromOk(null)));
    final playerBloc = PlayerBloc(playbackService, null, timeProvider);

    final song1 = song.rebuild((b) => b..duration = Duration(seconds: 3));
    final playlistBloc = PlaylistBloc("", <LibraryItem>[song1, song2], playerBloc, null, null);

    playerBloc.dispatch(SetPlaylist(playlistBloc));

    await for (var state in playerBloc.state.skip(1)) {
      expect(state.currentItem, playlistBloc.currentState.items.first);
      expect(state.playbackState, PlaybackState.Stopped);
      expect(state.remainedDuration, song1.duration);

      break;
    }

    playerBloc.dispatch(Play());

    await Future.delayed(timeProvider.adjustDuration(Duration(seconds: 5)));
    verify(playbackService.setItem(playlistBloc.currentState.items.first));
    expect(playerBloc.currentState.currentItem, equals(playlistBloc.currentState.items.last));
    await Future.delayed(timeProvider.adjustDuration(Duration(seconds: 4)));
    verify(playbackService.setItem(playlistBloc.currentState.items.last));
    verify(playbackService.stop());
  });

  test("PlayerBloc stops playing", () async {
    final playbackService = TestPlaybackService();
    when(playbackService.setItem(any)).thenAnswer((_) => Future.value(Result.fromOk(null)));
    when(playbackService.play()).thenAnswer((_) => Future.value(Result.fromOk(null)));
    when(playbackService.stop()).thenAnswer((_) => Future.value(Result.fromOk(null)));
    final playerBloc = PlayerBloc(playbackService, null, timeProvider);

    final playlistBloc = PlaylistBloc("", <LibraryItem>[song], playerBloc, null, null);

    playerBloc.dispatch(SetPlaylist(playlistBloc));

    await for (var state in playerBloc.state.skip(1)) {
      _verifyInitialState(state, playlistBloc, song);

      break;
    }

    playerBloc.dispatch(Play());
    verifyPlaying(playerBloc, song, playbackService, 2);
    playerBloc.dispatch(Stop());

    await for (var state in playerBloc.state.skip(2).take(1)) {
      verify(playbackService.stop());
      _verifyInitialState(state, playlistBloc, song);
    }

    playerBloc.dispatch(Play());

    verifyPlaying(playerBloc, song, playbackService, 2);
  });

  test("PlayerBloc starts playing requested song", () async {
    final playbackService = TestPlaybackService();
    when(playbackService.setItem(any)).thenAnswer((_) => Future.value(Result.fromOk(null)));
    when(playbackService.play()).thenAnswer((_) => Future.value(Result.fromOk(null)));
    when(playbackService.stop()).thenAnswer((_) => Future.value(Result.fromOk(null)));
    final playerBloc = PlayerBloc(playbackService, null, timeProvider);

    final playlistBloc = PlaylistBloc("", <LibraryItem>[song, song2, song3], playerBloc, null, null);

    playerBloc.dispatch(SetPlaylist(playlistBloc));

    await for (var state in playerBloc.state.skip(1)) {
      _verifyInitialState(state, playlistBloc, song);

      break;
    }

    playerBloc.dispatch(Play());
    verifyPlaying(playerBloc, song, playbackService, 2);
    playlistBloc.dispatch(PlayThisSong(song3));

    await for (var state in playerBloc.state.skip(2).take(1)) {
      verify(playbackService.stop());
      verifyPlaying(playerBloc, song3, playbackService, 2);
      break;
    }
  });
}

void _verifyInitialState(PlayerBlocState state, PlaylistBloc playlistBloc, LibraryItem song) {
  expect(state.currentItem, playlistBloc.currentState.items.first);
  expect(state.playbackState, PlaybackState.Stopped);
  expect(state.remainedDuration, song.duration);
}

class TestPlaybackService extends Mock implements IPlaybackService {}
