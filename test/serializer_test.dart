import 'package:beets_player/adapters/serializers.dart';
import 'package:beets_player/core/models/library_item.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test("serialization format", () {
    var item = LibraryItem((b) => b
      ..album = "taylor"
      ..artist = "taylor"
      ..duration = Duration.zero
      ..location = Uri.file("/Data/music")
      ..title = "nebulous");
    var s = serializers.serializeWith(LibraryItem.serializer, item);
    expect(s.toString(), "{artist: taylor, album: taylor, title: nebulous, duration: 0, location: file:///Data/music}");
    s as Map<String, dynamic>; //test to check compatibility with FormBuilder package
  });

  test("uri deserialization", () {
    var item = serializers.deserializeWith(LibraryItem.serializer,
        {"artist": "taylor", "album": "taylor", "title": "nebulous", "duration": 0, "location": "file:///Data/music"});
    expect(item.location, Uri.file("/Data/music"));

    item = serializers.deserializeWith(LibraryItem.serializer, {
      "artist": "taylor",
      "album": "taylor",
      "title": "nebulous",
      "duration": 0,
      "location": "http://youtup.com/hash"
    });
    expect(item.location, Uri.parse("http://youtup.com/hash"));
  });
}
