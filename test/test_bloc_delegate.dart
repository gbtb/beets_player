import 'package:bloc/bloc.dart';

///By default Bloc silently eats exceptions. This is unacceptable for testing.
class TestBlocDelegate extends BlocDelegate {
  @override
  void onError(Bloc bloc, Object error, StackTrace stacktrace) {
    throw error;
  }
}
