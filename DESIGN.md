Here I tried to formulate design goals & project plans for beets_player

# Beets Library Context

## Search

User should be able to search in **beets lib** by arbitrary strings as well as structured beets queries *Appling term **Query** to both categories*.
**Query results** represented as list of lib items, in basic form it should have artist-album-title-duration info on it.
**Query result** list can be transformed into playlist(merged into existing one) for playing through **Playback**, or saving.
Search results can (and should be cached), but not saved directly.

## Playlist
Collection of library items, which can be freely mutated, shuffled and persisted. **Playlists** have source sequence(*Better term required*) - it is ordered list of **Queries(and probably Query results)**, as well as user actions (add, del, reorder), which forms current state of **Playlist** - list of lib items. It should be possible to move through source sequence(history), at least to some extent. If source event is query, it can be *Frozen, Dynamic or Re-evaluated on demand*

## Playback
It plays items of single chosen playlist. It should have default player controls, such as:
1. Play/pause
1. Stop
1. Volume slider + mute button
1. Duration display (time left/total)
1. Next / prev song button
1. Shuffle button (which shuffle play order but not playlist itself)
1. Play once/loop button
1. Jump to current playlist (open tab with selected playlist)

Playback also should have an ability to play audio form YouTube.

## Library management
Basis of app is existing beets library. It should be possible to open info about song in tab(window) and look through full available info.
It should be possible to add youtube videos to library, either by command line or (better) by GUI of our player. For youtube videos should also be possible to specify start time and end time.

### Library Synchronization
There should be an function to copy local **beets library** from computer(**Master lib**) to mobile device, picking which files from library you actually want to copy.
Later, when some editing capability for lib will be introduced, there should be a way to sync changes back from phone to master.

## UX
* There should be available configurable hotkeys for Desktop version. And play/pause button should be focus independable! (tap space and music pauses no matter where focus in window currently is)



## App state management

### Settings
TBD when settings will be introduces

### Current state
Current state of app - playback, playlists, may be search results should be persisted after restart (in simplest form - as a dump file)


## First iteration of app

* Single page
* Player controls on top of page
* Horizontal tab bar below player. It has pinned search tab on the left.
* Search tab contains text field for **Queries**(simple search for now), start search button (as well as react on Enter keypress)
* After search completion, there should be available following actions:
    * Move to a new playlist +
    * Move to a new playlist and start playing +
    * Append to current playlist *Currently playing playlist? Or Selected playlist?* +
    * No editing (deleting of items) is possible under search tab!
* Each playlist gets a separate tab. In this tab song list gets displayed.
* If this playlist is playing now, currently playing song should be highlighted
* There should be available following actions:
    * Start playing current playlist +
    * Start playing current song
* Also simple playback with first 4 controls from list should be implemented +
* Simple state save with file dump
* All errors (expected and not expected) should be logged




