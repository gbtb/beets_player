use flutter_engine::plugins::prelude::*;
use flutter_engine::{channel::JsonMethodChannel, plugins::Plugin};
use gstreamer::prelude::*;
use log::{error, info, warn};
use std::sync::RwLock;
use std::sync::{Arc, Mutex};
use std::thread;

const PLUGIN_NAME: &str = module_path!();
const CHANNEL_NAME: &str = "player_plugin";

pub struct AudioPlayerPlugin {
    handler: Arc<RwLock<Handler>>,
}

struct Handler {
    player: gstreamer_player::Player,
}

impl Plugin for AudioPlayerPlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(JsonMethodChannel::new(CHANNEL_NAME, method_handler));
    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        _: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        info!("{:?}", call.method.as_str());
        match call.method.as_str() {
            "set_item" => {
                warn!("uri not decoded: {:?}", &call.args);
                if let Ok(uri) = from_value::<String>(&call.args) {
                    if let Some(loaded_uri) = self.player.get_uri() {
                        if loaded_uri.as_str() == uri {
                            info!("Item already was set: {}", uri);
                            return Ok(Value::Null);
                        }
                    }

                    info!("Item set: {}", uri);
                    // Tell the player what uri to play.
                    self.player.set_uri(&uri);
                    return Ok(Value::Null);
                }
                warn!("uri not decoded: {:?}", &call.args);
                return Ok(Value::Null);
            }
            "play" => {
                if let Some(_) = self.player.get_uri() {
                    self.player.play();
                    return Ok(Value::Null);
                }
                info!("Item was not set");
                return Ok(Value::Null);
            }
            "pause" => {
                self.player.pause();
                return Ok(Value::Null);
            }
            "stop" => {
                self.player.stop();
                return Ok(Value::Null);
            }
            "inc_volume" => {
                let mut volume = self.player.get_volume();
                volume = f64::min(1. as f64, volume + 0.05);
                self.player.set_volume(volume);
                return Ok(Value::Null);
            }
            "dec_volume" => {
                let mut volume = self.player.get_volume();
                volume = f64::max(0. as f64, volume - 0.05);
                self.player.set_volume(volume);
                return Ok(Value::Null);
            }
            "set_volume" => {
                if let Ok(volume) = from_value::<f64>(&call.args) {
                    if volume >= 0.0 && volume <= 1.0 {
                        self.player.set_volume(volume);
                        return Ok(Value::Null);
                    } else {
                        return Ok(Value::Null);
                        //TODO: error callback
                    }
                }
            }

            "mute" => {
                if let Ok(mute) = from_value::<bool>(&call.args) {
                    self.player.set_mute(mute);
                }
                return Ok(Value::Null);
            }

            _ => info!("Unknown method: {:?}", call.method.as_str()),
        }
        return Ok(Value::Null);
    }
}

impl AudioPlayerPlugin {
    pub fn init() -> Result<AudioPlayerPlugin, glib::error::Error> {
        gstreamer::init()?;

        let main_loop = glib::MainLoop::new(None, false);

        let dispatcher = gstreamer_player::PlayerGMainContextSignalDispatcher::new(None);
        let player = gstreamer_player::Player::new(
            None,
            Some(&dispatcher.upcast::<gstreamer_player::PlayerSignalDispatcher>()),
        );

        let error = Arc::new(Mutex::new(Ok(())));

        let main_loop_clone = main_loop.clone();
        // Connect to the player's "end-of-stream" signal, which will tell us when the
        // currently played media stream reached its end.
        player.connect_end_of_stream(move |player| {
            let main_loop = &main_loop_clone;
            player.stop();
            //main_loop.quit();
        });

        let main_loop_clone = main_loop.clone();
        let error_clone = Arc::clone(&error);
        // Connect to the player's "error" signal, which will inform us about eventual
        // errors (such as failing to retrieve a http stream).
        player.connect_error(move |player, err| {
            let main_loop = &main_loop_clone;
            let error = &error_clone;

            *error.lock().unwrap() = Err(err.clone());

            error!("Error while playing: {:?}", err.clone());

            player.stop();
            //main_loop.quit();
        });

        let main_loop_clone = main_loop.clone();
        thread::spawn(move || {
            &main_loop_clone.run();
        });

        // let plugin = AudioPlayerPlugin {
        //     channel: JsonMethodChannel::new("beets_player/player"),
        //     player: player,
        //     main_loop: main_loop,
        // };

        let plugin = AudioPlayerPlugin {
            handler: Arc::new(RwLock::new(Handler { player: player })),
        };

        Ok(plugin)
    }
}
