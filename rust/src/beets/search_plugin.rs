use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;
use flutter_engine::plugins::prelude::*;
use flutter_engine::{channel::JsonMethodChannel, plugins::Plugin};
use log::info;
use serde::{Serialize, Serializer};
use std::fmt::Display;
use std::str;

use super::schema::items::dsl::*;

const PLUGIN_NAME: &str = module_path!();
const CHANNEL_NAME: &str = "search_plugin";

pub struct BeetsPlugin {
    handler: Arc<RwLock<Handler>>,
}

#[derive(Default)]
struct Handler {
    database_uri: Option<String>,
}

#[derive(Serialize)]
enum SearchPluginError {
    IncorrectQuery,
    DatabaseUriNotSet,
}

impl Display for SearchPluginError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            SearchPluginError::IncorrectQuery => write!(f, "Incorrect search query"),
            SearchPluginError::DatabaseUriNotSet => {
                write!(f, "Database Uri must be set prior to search")
            }
        }
    }
}

impl Into<MethodCallError> for SearchPluginError {
    fn into(self) -> MethodCallError {
        MethodCallError::CustomError {
            code: "1".to_string(),
            message: self.to_string(),
            details: json_value!(self),
        }
    }
}

impl Default for BeetsPlugin {
    fn default() -> Self {
        Self {
            handler: Arc::new(RwLock::new(Handler::default())),
        }
    }
}

fn establish_connection(database_uri: &str) -> SqliteConnection {
    SqliteConnection::establish(&database_uri)
        .expect(&format!("Error connecting to {}", database_uri))
}

/// Beets lib item
/// (Order of fields must match with schema)
#[derive(Queryable, Serialize)]
pub struct Item {
    pub title: Option<String>,
    pub artist: Option<String>,
    pub id: i32,
    pub album: Option<String>,

    #[serde(serialize_with = "serialize_path", rename = "location")]
    pub path: Option<Vec<u8>>,
    ///beets stores track length as number of seconds
    #[serde(serialize_with = "serialize_length", rename = "duration")]
    pub length: Option<f32>,
    //pub body: String,
    //pub published: bool,
}

fn serialize_length<S>(c: &Option<f32>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let duration = c.map(|l| (l * 1e6).ceil() as u32);
    match duration {
        Some(d) => serializer.serialize_some(&d),
        None => serializer.serialize_none(),
    }
}

fn serialize_path<S>(c: &Option<Vec<u8>>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    match c {
        Some(bytes) => match String::from_utf8(bytes.to_vec()).ok() {
            Some(uri) => serializer.serialize_some(&uri),
            None => serializer.serialize_none(),
        },
        None => serializer.serialize_none(),
    }
}

impl Plugin for BeetsPlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(JsonMethodChannel::new(CHANNEL_NAME, method_handler));
    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        _: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        println!("Hello");
        match call.method.as_str() {
            "simple_search" => {
                if let Ok(query) = from_value::<String>(&call.args) {
                    if let Some(database_uri) = &self.database_uri {
                        let connection = establish_connection(&database_uri);
                        let results = items
                            .filter(
                                title
                                    .like(format!("%{}%", query))
                                    .or(artist.like(format!("%{}%", query)))
                                    .or(album.like(format!("%{}%", query))),
                            )
                            .load::<Item>(&connection)
                            .expect("Error loading posts");

                        info!("{}-{}", query, results.len());

                        let result = Ok(json_value!(results));

                        result
                    } else {
                        Err(SearchPluginError::DatabaseUriNotSet.into())
                    }
                } else {
                    Err(MethodCallError::ArgParseError(MethodArgsError::WrongType(
                        "query".to_string(),
                        call.args,
                    )))
                }
            }
            "init" => {
                if let Ok(uri) = from_value::<String>(&call.args) {
                    info!("init {}", uri);
                    self.database_uri = Some(uri);
                    Ok(Value::Null)
                } else {
                    Err(MethodCallError::ArgParseError(MethodArgsError::WrongType(
                        "database_uri".to_string(),
                        call.args,
                    )))
                }
            }
            _ => Err(MethodCallError::NotImplemented),
        }
    }
}
