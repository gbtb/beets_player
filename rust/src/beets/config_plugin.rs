use flutter_engine::plugins::prelude::*;
use flutter_engine::{channel::JsonMethodChannel, plugins::Plugin};
use log::{error, info, warn};
use serde::Serialize;
use std::env;
use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use std::str;

const PLUGIN_NAME: &str = module_path!();
const CHANNEL_NAME: &str = "beets_config_plugin";

pub struct BeetsConfigPlugin {
    handler: Arc<RwLock<Handler>>,
}

struct Handler;

#[derive(Serialize, Debug)]
enum ConfigPluginError {
    UnknownError(String),
    FilesNotFound,
    FileNotFound(),
    CantDeserialize,
}

impl From<serde_yaml::Error> for ConfigPluginError {
    fn from(_: serde_yaml::Error) -> Self {
        ConfigPluginError::CantDeserialize
    }
}

impl From<std::io::Error> for ConfigPluginError {
    fn from(err: std::io::Error) -> Self {
        match err.kind() {
            std::io::ErrorKind::NotFound => ConfigPluginError::FileNotFound(),
            _ => ConfigPluginError::UnknownError(err.description().to_string()),
        }
    }
}

impl Default for BeetsConfigPlugin {
    fn default() -> Self {
        Self {
            handler: Arc::new(RwLock::new(Handler)),
        }
    }
}

#[derive(Deserialize, Serialize, Debug)]
pub struct BeetsConfig {
    pub library: String,
}

impl Plugin for BeetsConfigPlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(JsonMethodChannel::new(CHANNEL_NAME, method_handler));
    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        _: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        match call.method.as_str() {
            "find_config" => {
                //todo: other platforms (aka windows)
                let config = vec![
                    env::var("BEETSDIR"),
                    env::var("HOME").map(|home| format!("{}/.config/beets/config.yaml", home)),
                    env::var("XDG_CONFIG_DIR").map(|conf| format!("{}/beets/config.yaml", conf)),
                ]
                .iter()
                .filter(|res| res.is_ok())
                .map(|path| {
                    let p = path.clone().unwrap();
                    try_read(&p)
                })
                .skip_while(|conf| conf.is_err())
                .next();

                if let Some(conf) = config {
                    Ok(json_value!(conf.unwrap()))
                } else {
                    Ok(Value::Null)
                }
            }
            "get_config" => {
                if let Ok(path) = from_value::<String>(&call.args) {
                    if let Ok(conf) = try_read(&path) {
                        Ok(json_value!(conf))
                    } else {
                        Ok(json_value!(Value::Null))
                    }
                } else {
                    Err(MethodCallError::ArgParseError(MethodArgsError::WrongType(
                        "path".to_string(),
                        call.args,
                    )))
                }
            }
            _ => Err(MethodCallError::NotImplemented),
        }
    }
}

fn try_read(possible_config_path: &str) -> Result<BeetsConfig, ConfigPluginError> {
    let f = File::open(possible_config_path)?;
    let reader = BufReader::new(f);

    serde_yaml::from_reader(reader).map_err(ConfigPluginError::from)
}
