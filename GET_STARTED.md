# after fresh container start
```
flutter pub get
```

# run app

For `ALSA_CARD` variable value you should run `aplay -l` in host system and pick your device name.

```
export ALSA_CARD=PCH; ./scripts/run.py
```

# run codegen

Codegen commands, required for built_value library, are attached to default vscode build task, so it can be triggered by `ctrl-shift-b` and then it keeps running in watch mode.

# git folder checkout
Usefull for copying subcrates of flutter-rs from master. 

`svn checkout https://github.com/gliheng/flutter-rs/trunk/flutter-plugins`